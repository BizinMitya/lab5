<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 10.04.2016
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Импорт записей</title>
</head>
<body>
<a href="companies.jsp">На главную</a>
<form action="ImportServlet" method="post" target="_blank" enctype="multipart/form-data">
    <input type="file" id="file" onchange="if (document.getElementById('file').value)
                            document.getElementById('button').disabled = false;" name="file" accept="text/xml">
    <input id="button" type="submit" disabled="disabled" value="Загрузить"><br>
    Что делать с дубликатами записей?<br>
    <input type="radio" value="miss" name="duplicate" checked="checked">Пропустить<br>
    <input type="radio" value="rewrite" name="duplicate">Перезаписать<br>
</form>
</body>
</html>
