<%@ page import="controllers.CompanyController" %>
<%@ page import="controllers.DepartmentController" %>
<%@ page import="controllers.impl.CompanyControllerImpl" %>
<%@ page import="controllers.impl.DepartmentControllerImpl" %>
<%@ page import="model.Company" %>
<%@ page import="model.Department" %>
<%@ page import="model.Models" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Компании</title>
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
    <script>
        function search() {
            window.open("search.jsp", "Поиск", "width=800,height=600,resizable=no,scrollbars=yes,status=yes");
        }
    </script>
</head>
<body>
<a href="companies.jsp">На главную</a><br>
<a href="export.jsp">Экспорт записей</a><br>
<a href="import.jsp">Импорт записей</a><br>
<%
    request.setCharacterEncoding("UTF-8");
    String id = request.getParameter("id");
    String delete = request.getParameter("delete");
    CompanyController companyController = new CompanyControllerImpl();
    DepartmentController departmentController = new DepartmentControllerImpl();
    if (request.getMethod().equalsIgnoreCase("POST")) {
        if (id == null) {
            try {
                companyController.insert(new Company(0, request.getParameter("name"), request.getParameter("description")));
                response.sendRedirect("companies.jsp");
            } catch (NamingException | SQLException e) {
                e.printStackTrace();
            }
        } else if (id != null && delete == null) {
            try {
                Company company = new Company(Integer.parseInt(id), request.getParameter("name"), request.getParameter("description"));
                companyController.update(company);
                response.sendRedirect("companies.jsp?id=" + id);
            } catch (NamingException | SQLException e) {
                e.printStackTrace();
            }
        } else if (id != null && delete != null) {
            try {
                companyController.delete(Integer.parseInt(delete));
                response.sendRedirect("companies.jsp");
            } catch (NamingException | SQLException e) {
                e.printStackTrace();
            }
        }
    } else if (request.getMethod().equalsIgnoreCase("GET")) {
        if (id != null) {
            Company company = companyController.findOne(Integer.parseInt(id));
            out.println(Models.drawPath(company));
            List<Department> departments = departmentController.findAll();
            out.println("<center>\n" +
                    "<form class=\"form-inline\" action=\"companies.jsp\" method=\"POST\">\n" +
                    "<input type=\"hidden\" name=\"id\" value=" + id + ">" +
                    "Имя: <input name=\"name\" value=\"" + company.getName() + "\" type=\"text\">\n" +
                    "Описание: <input name=\"description\" value=\"" + company.getDescription() + "\" type=\"text\">\n" +
                    "<input type=\"submit\" value=\"Изменить компанию\"></form>" +
                    "<form name=\"add_form\" class=\"form-inline\" action=\"departments.jsp\" method=\"POST\">" +
                    "<input type=\"hidden\" name=\"idCompany\" value=" + id + ">" +
                    "Имя: <input name=\"name\" type=\"text\">\n" +
                    "Описание: <input name=\"description\" type=\"text\">\n" +
                    "Босс: <input name=\"boss\" id=\"boss\" type=\"text\">\n" +
                    "<input type=\"hidden\" id=\"bossHidden\" name=\"bossHidden\">" +
                    "<button type=\"button\" onclick=\"search()\">Выбрать</button>\n" +
                    "<input type=\"submit\" value=\"Добавить департамент\"></form>");
            out.println("<table class=\"table table-bordered\"><th colspan=\"2\">Департаменты</th><tr><td>" + company.getName() + "</td><td>" + company.getDescription() + "</td></tr>");
            for (Department department : departments) {
                if (department.getCompany().getId().equals(company.getId())) {
                    out.println("<tr><th>" +
                            "<a href=departments.jsp?id=" + department.getId() + ">" + department.getName() + "<a></th><th>" +
                            "<form class=\"form-inline\" action=\"departments.jsp\" method=\"POST\">\n" +
                            "<input type=\"hidden\" name=\"id\" value=" + department.getId() + ">" +
                            "<input type=\"hidden\" name=\"delete\" value=" + department.getId() + ">" +
                            "<input type=\"hidden\" name=\"idCompany\" value=" + id + ">" +
                            "<input type=\"submit\" value=\"Удалить\">\n" +
                            "</form>" +
                            "</th></tr>");
                }
            }
            out.println("</table>");
        } else {
            List<Company> companies = companyController.findAll();
            out.println("<center>\n" +
                    "<form class=\"form-inline\" url=\"companies.jsp\" method=\"POST\">\n" +
                    "Имя: <input name=\"name\" type=\"text\">\n" +
                    "Описание: <input name=\"description\" type=\"text\">\n" +
                    "<input id=\"addButton\" type=\"submit\" value=\"Добавить\">" +
                    "</form>\n" +
                    "<table class=\"table table-bordered\">\n" +
                    "<tr><th style=\"font: bold 14pt/12pt Verdana;\" colspan=\"2\">Компании</th></tr>");
            for (Company company : companies) {
                out.println("<tr><th><a href=companies.jsp?id=" + company.getId() + ">" + company.getName() + "</a></th><th>" +
                        "<form class=\"form-inline\" action=\"companies.jsp\" method=\"POST\">\n" +
                        "<input type=\"hidden\" name=\"id\" value=" + company.getId() + ">" +
                        "<input type=\"hidden\" name=\"delete\" value=" + company.getId() + ">" +
                        "<input type=\"submit\" value=\"Удалить\">\n" +
                        "</form>\n" +
                        "</th></tr>");
            }
            out.println("</table></center>");
        }
    }
%>
</body>
</html>
