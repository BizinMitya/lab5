<%@ page import="controllers.DepartmentController" %>
<%@ page import="controllers.impl.DepartmentControllerImpl" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 10.04.2016
  Time: 12:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<%
    request.setCharacterEncoding("UTF-8");
    String query = request.getParameter("q");
    response.setHeader("Content-Type", "text/html");
    DepartmentController departmentController = new DepartmentControllerImpl();
    List<String> list = departmentController.findByLikeParameter("name", query);
    String[] strings = new String[list.size()];
    strings = list.toArray(strings);
    int cnt = 1;
    for (int i = 0; i < strings.length; i++) {
        if (strings[i].toUpperCase().startsWith(query.toUpperCase())) {
            out.print(strings[i] + "\n");
            if (cnt >= 10)
                break;
            cnt++;
        }
    }
%>
