<%@ page import="controllers.EmployeeController" %>
<%@ page import="controllers.impl.EmployeeControllerImpl" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<%
    //autocomplete по surname
    request.setCharacterEncoding("UTF-8");
    String query = request.getParameter("q");
    response.setHeader("Content-Type", "text/html");
    EmployeeController employeeController = new EmployeeControllerImpl();
    List<String> list = employeeController.findByLikeParameter("surname", query);
    String[] strings = new String[list.size()];
    strings = list.toArray(strings);
    int cnt = 1;
    for (int i = 0; i < strings.length; i++) {
        if (strings[i].toUpperCase().startsWith(query.toUpperCase())) {
            out.print(strings[i] + "\n");
            if (cnt >= 10)
                break;
            cnt++;
        }
    }
%>

