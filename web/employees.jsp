<%@ page import="controllers.DepartmentController" %>
<%@ page import="controllers.EmployeeController" %>
<%@ page import="controllers.impl.DepartmentControllerImpl" %>
<%@ page import="controllers.impl.EmployeeControllerImpl" %>
<%@ page import="model.Employee" %>
<%@ page import="model.Models" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="java.sql.SQLException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Сотрудники</title>
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
    <script>
        function searchEmp() {
            window.open("searchEmp.jsp", "Поиск", "width=800,height=600,resizable=no,scrollbars=yes,status=yes");
        }
    </script>
</head>
<body>
<a href="companies.jsp">На главную</a>
<%
    request.setCharacterEncoding("UTF-8");
    String id = request.getParameter("id");
    String idCompany = request.getParameter("idCompany");
    String idDepartment = request.getParameter("idDepartment");
    String delete = request.getParameter("delete");
    EmployeeController employeeController = new EmployeeControllerImpl();
    DepartmentController departmentController = new DepartmentControllerImpl();
    if (request.getMethod().equalsIgnoreCase("POST")) {
        if (id == null) {
            try {
                String boss = request.getParameter("empBossHidden");
                if (boss.equals("")) {//если не указан босс у сотрудника,то без босса(можно сам себе босс поставить)
                    String name = request.getParameter("name");
                    String surname = request.getParameter("surname");
                    Employee employee = new Employee(0, name, surname,
                            departmentController.findOne(Integer.parseInt(idDepartment)), null);
                    employeeController.insert(employee);
                } else {
                    employeeController.insert(new Employee(0, request.getParameter("name"), request.getParameter("surname"),
                            departmentController.findOne(Integer.parseInt(idDepartment)), employeeController.findOne(Integer.parseInt(boss))));
                }
                response.sendRedirect("departments.jsp?id=" + idDepartment);
            } catch (NamingException | SQLException | NumberFormatException e) {
                e.printStackTrace();
            }

        } else if (id != null && delete == null) {
            try {
                String empBossHidden = request.getParameter("empBossHidden");
                Employee employee = new Employee(Integer.parseInt(id), request.getParameter("name"), request.getParameter("surname"),
                        employeeController.findOne(Integer.parseInt(id)).getDepartment(), employeeController.findOne(Integer.parseInt(empBossHidden)));
                employeeController.update(employee);
                response.sendRedirect("employees.jsp?id=" + id);
            } catch (NamingException | SQLException e) {
                e.printStackTrace();
            }
        } else if (id != null && delete != null) {
            try {
                employeeController.delete(Integer.parseInt(delete));
                response.sendRedirect("departments.jsp?id=" + idDepartment);
            } catch (NamingException | SQLException e) {
                e.printStackTrace();
            }
        }
    } else if (request.getMethod().equalsIgnoreCase("GET")) {
        if (id != null) {
            Employee employee = employeeController.findOne(Integer.parseInt(id));
            out.println(Models.drawPath(employee));
            out.println("<center>\n" +
                    "<form name=\"change_form\" class=\"form-inline\" action=\"employees.jsp\" method=\"POST\">" +
                    "<input type=\"hidden\" name=\"idCompany\" value=" + idCompany + ">" +
                    "<input type=\"hidden\" name=\"idDepartment\" value=" + idDepartment + ">" +
                    "<input type=\"hidden\" name=\"id\" value=" + id + ">" +
                    "Имя: <input name=\"name\" type=\"text\" value=\"" + employee.getName() + "\">\n" +
                    "Фамилия: <input name=\"surname\" type=\"text\" value=\"" + employee.getSurname() + "\">\n");
            if (employee.getEmployee() == null) {//нет босса
                out.println("Босс: <input name=\"empBoss\" id=\"empBoss\" type=\"text\">\n" +
                        "<input type=\"hidden\" name=\"empBossHidden\" id=\"empBossHidden\">");
            } else {
                out.println("Босс: <input name=\"empBoss\" id=\"empBoss\" type=\"text\" value=\"" + employee.getEmployee().getName() + " " + employee.getEmployee().getSurname() + "\">\n" +
                        "<input type=\"hidden\" name=\"empBossHidden\" id=\"empBossHidden\" value=\"" + employee.getEmployee().getId() + "\">");
            }
            out.println("<button type=\"button\" onclick=\"searchEmp()\">Выбрать</button>\n" +
                    "<input type=\"submit\" value=\"Изменить сотрудника\"></form>");
            out.println("<table class=\"table table-bordered\"><tr><th>Подчинённые</th></tr><tr><td>");
            employeeController.helper(employee.getId(), employeeController.findAll(), employee.getDepartment().getId());
            out.println(employeeController.buildTree(employeeController.getResult(), employee.getId(), new StringBuffer()));
            out.println("</td></tr></table>");
        }
    }
%>
</body>
</html>
