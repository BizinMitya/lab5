<%@ page import="controllers.EmployeeController" %>
<%@ page import="controllers.impl.EmployeeControllerImpl" %>
<%@ page import="model.Employee" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Поиск</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
    <script type="text/javascript" src="JS/jquery-1.4.2.min.js"></script>
    <script src="JS/jquery.autocomplete.js" type="text/javascript"></script>
    <script type="text/javascript">
        function send(number) {
            var table = document.body.children[1];
            window.opener.document.getElementById('bossHidden').value = table.rows[number + 1].cells[0].innerHTML;
            window.opener.document.getElementById('boss').value = table.rows[number + 1].cells[1].innerHTML + " " + table.rows[number + 1].cells[2].innerHTML;
            window.close();
        }
    </script>
</head>
<body>
<form class="form-inline" action="search.jsp" method="POST">
    id: <input id="id" name="id" type="text">
    Имя: <input id="name" name="name" type="text">
    Фамилия: <input id="surname" name="surname" type="text">
    <input id="search" type="submit" value="Показать">
</form>
<%
    request.setCharacterEncoding("UTF-8");
    String id = request.getParameter("id");
    String name = request.getParameter("name");
    String surname = request.getParameter("surname");
    EmployeeController employeeController = new EmployeeControllerImpl();
    if (request.getMethod().equalsIgnoreCase("POST")) {
        List<Employee> employees = employeeController.findByParameters(id, name, surname);
        out.println("<table class=\"table table-bordered\"><tr><td>id</td><td>Имя</td><td>Фамилия</td><td>Выбор</td></tr>");
        for (int i = 0; i < employees.size(); i++) {
            out.println("<tr>" +
                    "<td>" + employees.get(i).getId() + "</td>" +
                    "<td>" + employees.get(i).getName() + "</td>" +
                    "<td>" + employees.get(i).getSurname() + "</td>" +
                    "<td><input type=\"submit\" value=\"Отправить\" onclick=\"send(" + i + ")\"></td></tr>");
        }
        out.println("</table></center>");
    }
%>
<script>
    jQuery(function () {
        $("#id").autocomplete("autocomplete/searchIdEmployee.jsp", {delay: 100});
        $("#name").autocomplete("autocomplete/searchNameEmployee.jsp", {delay: 100});
        $("#surname").autocomplete("autocomplete/searchSurnameEmployee.jsp", {delay: 100});
    });
</script>
</body>
</html>
