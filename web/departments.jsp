<%@ page import="controllers.CompanyController" %>
<%@ page import="controllers.DepartmentController" %>
<%@ page import="controllers.EmployeeController" %>
<%@ page import="controllers.impl.CompanyControllerImpl" %>
<%@ page import="controllers.impl.DepartmentControllerImpl" %>
<%@ page import="controllers.impl.EmployeeControllerImpl" %>
<%@ page import="model.Department" %>
<%@ page import="model.Employee" %>
<%@ page import="model.Models" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <title>Департаменты</title>
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
    <script>
        function search() {
            window.open("search.jsp", "Поиск", "width=800,height=600,resizable=no,scrollbars=yes,status=yes");
        }
        function searchEmp() {
            window.open("searchEmp.jsp", "Поиск", "width=800,height=600,resizable=no,scrollbars=yes,status=yes");
        }
    </script>
</head>
<body>
<a href="companies.jsp">На главную</a>
<%
    //Добавление департамента возможно без босса,но изменение департамента ОБЯЗАТЕЛЬНО с выбранным боссом
    request.setCharacterEncoding("UTF-8");
    String id = request.getParameter("id");
    String idCompany = request.getParameter("idCompany");
    String delete = request.getParameter("delete");
    CompanyController companyController = new CompanyControllerImpl();
    DepartmentController departmentController = new DepartmentControllerImpl();
    EmployeeController employeeController = new EmployeeControllerImpl();
    if (request.getMethod().equalsIgnoreCase("POST")) {
        if (id == null) {
            try {
                String boss = request.getParameter("bossHidden");
                if (boss.equals("")) {
                    departmentController.insert(new Department(0, request.getParameter("name"), request.getParameter("description"),
                            companyController.findOne(Integer.parseInt(idCompany)), null));
                } else {
                    departmentController.insert(new Department(0, request.getParameter("name"), request.getParameter("description"),
                            companyController.findOne(Integer.parseInt(idCompany)), employeeController.findOne(Integer.parseInt(boss))));
                }
                response.sendRedirect("companies.jsp?id=" + idCompany);
            } catch (NamingException | SQLException | NumberFormatException e) {
                e.printStackTrace();
            }
        } else if (id != null && delete == null) {
            try {
                String bossHidden = request.getParameter("bossHidden");
                Department department = new Department(Integer.parseInt(id), request.getParameter("name"), request.getParameter("description"),
                        departmentController.findOne(Integer.parseInt(id)).getCompany(), employeeController.findOne(Integer.parseInt(bossHidden)));
                departmentController.update(department);
                response.sendRedirect("departments.jsp?id=" + id);
            } catch (NamingException | SQLException e) {
                e.printStackTrace();
            }
        } else if (id != null && delete != null) {
            try {
                departmentController.delete(Integer.parseInt(delete));
                response.sendRedirect("companies.jsp?id=" + idCompany);
            } catch (NamingException | SQLException e) {
                e.printStackTrace();
            }
        }
    } else if (request.getMethod().equalsIgnoreCase("GET")) {
        if (id != null) {
            Department department = departmentController.findOne(Integer.parseInt(id));
            out.println(Models.drawPath(department));
            List<Employee> employees = employeeController.findAll();
            out.println("<center>\n" +
                    "<form name=\"change_form\" class=\"form-inline\" action=\"departments.jsp\" method=\"POST\">" +
                    "<input type=\"hidden\" name=\"idCompany\" value=" + idCompany + ">" +
                    "<input type=\"hidden\" name=\"id\" value=" + id + ">" +
                    "Имя: <input name=\"name\" type=\"text\" value=\"" + department.getName() + "\">\n" +
                    "Описание: <input name=\"description\" type=\"text\" value=\"" + department.getDescription() + "\">\n");
            if (department.getEmployee() == null) {
                out.println("Босс: <input name=\"boss\" id=\"boss\" type=\"text\">\n" +
                        "<input type=\"hidden\" name=\"bossHidden\" id=\"bossHidden\">");
            } else {
                out.println("Босс: <input name=\"boss\" id=\"boss\" type=\"text\" value=\"" + department.getEmployee().getName() + " " + department.getEmployee().getSurname() + "\">\n" +
                        "<input type=\"hidden\" name=\"bossHidden\" id=\"bossHidden\" value=\"" + department.getEmployee().getId() + "\">");
            }
            out.println("<button type=\"button\" onclick=\"search()\">Выбрать</button>\n" +
                    "<input type=\"submit\" value=\"Изменить департамент\"></form>" +
                    "<form name=\"add_form\" class=\"form-inline\" action=\"employees.jsp\" method=\"POST\">" +
                    "<input type=\"hidden\" name=\"idCompany\" value=" + idCompany + ">" +
                    "<input type=\"hidden\" name=\"idDepartment\" value=" + id + ">" +
                    "Имя: <input name=\"name\" type=\"text\">\n" +
                    "Фамилия: <input name=\"surname\" type=\"text\">\n" +
                    "Босс: <input name=\"empBoss\" id=\"empBoss\" type=\"text\">\n" +
                    "<input type=\"hidden\" name=\"empBossHidden\" id=\"empBossHidden\">" +
                    "<button type=\"button\" onclick=\"searchEmp()\">Выбрать</button>\n" +
                    "<input type=\"submit\" value=\"Добавить сотрудника\"></form>");
            if (department.getEmployee() == null) {
                out.println("<table class=\"table table-bordered\"><th colspan=\"3\">Сотрудники</th><tr><td>" + department.getName() + "</td><td>" +
                        department.getDescription() + "</td><td>" + "Нет босса" + "</td></tr>");
            } else {
                out.println("<table class=\"table table-bordered\"><th colspan=\"3\">Сотрудники</th><tr><td>" + department.getName() + "</td><td>" +
                        department.getDescription() + "</td><td>" + employeeController.findOne(department.getEmployee().getId()).getName() +
                        " " + employeeController.findOne(department.getEmployee().getId()).getSurname() + "</td></tr>");
            }
            for (Employee employee : employees) {
                if (employee.getDepartment().getId().equals(Integer.parseInt(id))) {
                    out.println("<tr><td colspan=\"2\"><a href=\"employees.jsp?id=" + employee.getId() + "\">" + employee.getName() + " " + employee.getSurname() + "</a></td>" +
                            "<td><form class=\"form-inline\" action=\"employees.jsp\" method=\"POST\">" +
                            "<input type=\"hidden\" name=\"id\" value=" + employee.getId() + ">" +
                            "<input type=\"hidden\" name=\"delete\" value=" + employee.getId() + ">" +
                            "<input type=\"hidden\" name=\"idDepartment\" value=" + employee.getDepartment().getId() + ">" +
                            "<input type=\"hidden\" name=\"idCompany\" value=" + departmentController.findOne(employee.getDepartment().getId()).getCompany().getId() + ">" +
                            "<input type=\"submit\" value=\"Удалить\"/></form>" +
                            "</td></tr>");
                }
            }
            out.println("</table>");
        }
    }
%>
</body>
</html>
