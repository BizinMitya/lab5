<%@ page import="controllers.CompanyController" %>
<%@ page import="controllers.DepartmentController" %>
<%@ page import="controllers.EmployeeController" %>
<%@ page import="controllers.impl.CompanyControllerImpl" %>
<%@ page import="controllers.impl.DepartmentControllerImpl" %>
<%@ page import="controllers.impl.EmployeeControllerImpl" %>
<%@ page import="model.Company" %>
<%@ page import="model.Department" %>
<%@ page import="model.Employee" %>
<%@ page import="java.util.List" %>
<%@ page errorPage="error.jsp" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 10.04.2016
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Экспорт записей</title>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css">
    <script type="text/javascript" src="JS/jquery-1.4.2.min.js"></script>
    <script src="JS/jquery.autocomplete.js" type="text/javascript"></script>
</head>
<body>
<a href="companies.jsp">На главную</a>
<form class="form-inline" action="export.jsp" method="POST">
    id компании: <input id="idIdCompany" name="idCompany" type="text">
    Имя компании: <input id="idNameCompany" name="nameCompany" type="text">
    Описание компании: <input id="idDescriptionCompany" name="descriptionCompany" type="text">
    <input name="searchCompany" type="submit" value="Показать">
</form>
<form class="form-inline" action="export.jsp" method="POST">
    id департамента: <input id="idIdDepartment" name="idDepartment" type="text">
    Имя департамента: <input id="idNameDepartment" name="nameDepartment" type="text">
    Описание департамента: <input id="idDescriptionDepartment" name="descriptionDepartment" type="text">
    <input name="searchDepartment" type="submit" value="Показать">
</form>
<form class="form-inline" action="export.jsp" method="POST">
    id сотрудника: <input id="idIdEmployee" name="idEmployee" type="text">
    Имя сотрудника: <input id="idNameEmployee" name="nameEmployee" type="text">
    Фамилия сотрудника: <input id="idSurnameEmployee" name="surnameEmployee" type="text">
    <input name="searchEmployee" type="submit" value="Показать">
</form>

<%
    request.setCharacterEncoding("UTF-8");
    String idCompany = request.getParameter("idCompany");
    String nameCompany = request.getParameter("nameCompany");
    String descriptionCompany = request.getParameter("descriptionCompany");
    String idDepartment = request.getParameter("idDepartment");
    String nameDepartment = request.getParameter("nameDepartment");
    String descriptionDepartment = request.getParameter("descriptionDepartment");
    String idEmployee = request.getParameter("idEmployee");
    String nameEmployee = request.getParameter("nameEmployee");
    String surnameEmployee = request.getParameter("surnameEmployee");
    String searchCompany = request.getParameter("searchCompany");
    String searchDepartment = request.getParameter("searchDepartment");
    String searchEmployee = request.getParameter("searchEmployee");
    CompanyController companyController = new CompanyControllerImpl();
    DepartmentController departmentController = new DepartmentControllerImpl();
    EmployeeController employeeController = new EmployeeControllerImpl();
    if (request.getMethod().equalsIgnoreCase("POST")) {
        if (searchCompany != null) {
            List<Company> companies = companyController.findByParameters(idCompany, nameCompany, descriptionCompany);
            out.println("<center><form action=\"ExportServlet\" target=\"_blank\" method=\"POST\"><table class=\"table table-bordered\"><tr><td>id</td><td>Имя</td><td>Описание</td></tr>");
            out.println("<input type=\"hidden\" id=\"idType\" name=\"type\" value=\"companies\">");//передача типа таблицы
            out.println("<input type=\"hidden\" id=\"idSizeCompany\" name=\"sizeCompany\" value=" + companies.size() + ">");//передача размера таблицы
            for (int i = 0; i < companies.size(); i++) {
                out.println("<tr>" +
                        "<td>" + companies.get(i).getId() + "<input type=\"hidden\" id=\"idIdCompany" + i + "\" name=\"idCompany" + i + "\" value=" + companies.get(i).getId() + ">" + "</td>" +
                        "<td>" + companies.get(i).getName() + "</td>" +
                        "<td>" + companies.get(i).getDescription() + "</td>" +
                        "</tr>");
            }
            if (companies.size() != 0) {
                out.println("</table>");
                out.println("<input type=\"radio\" value=\"down\" name=\"dependence\">Вниз по иерархии<br>");
                out.println("<input type=\"radio\" value=\"current\" name=\"dependence\" checked=\"checked\">Без зависимостей<br>");
                out.println("<input type=\"radio\" value=\"html\" name=\"view\" checked=\"checked\">Представить в виде html<br>");
                out.println("<input type=\"radio\" value=\"xml\" name=\"view\">Представить в виде xml<br>");
                out.println("<input type=\"submit\" value=\"Экспорт\">");
                out.println("</form></center>");
            } else {
                out.println("</table>");
                out.println("</form></center>");
            }
        }
        if (searchDepartment != null) {
            List<Department> departments = departmentController.findByParameters(idDepartment, nameDepartment, descriptionDepartment);
            out.println("<center><form action=\"ExportServlet\" target=\"_blank\" method=\"POST\"><table class=\"table table-bordered\"><tr><td>id</td><td>Имя</td><td>Описание</td></tr>");
            out.println("<input type=\"hidden\" id=\"idType\" name=\"type\" value=\"departments\">");//передача типа таблицы
            out.println("<input type=\"hidden\" id=\"idSizeDepartment\" name=\"sizeDepartment\" value=" + departments.size() + ">");//передача размера таблицы
            for (int i = 0; i < departments.size(); i++) {
                out.println("<tr>" +
                        "<td>" + departments.get(i).getId() + "<input type=\"hidden\" id=\"idIdDepartment" + i + "\" name=\"idDepartment" + i + "\" value=" + departments.get(i).getId() + ">" + "</td>" +
                        "<td>" + departments.get(i).getName() + "</td>" +
                        "<td>" + departments.get(i).getDescription() + "</td>" +
                        "</tr>");
            }
            if (departments.size() != 0) {
                out.println("</table>");
                out.println("<input type=\"radio\" value=\"up\" name=\"dependence\">Вверх по иерархии<br>");
                out.println("<input type=\"radio\" value=\"down\" name=\"dependence\">Вниз по иерархии<br>");
                out.println("<input type=\"radio\" value=\"all\" name=\"dependence\">В обе стороны<br>");
                out.println("<input type=\"radio\" value=\"current\" name=\"dependence\" checked=\"checked\">Без зависимостей<br>");
                out.println("<input type=\"radio\" value=\"html\" name=\"view\" checked=\"checked\">Представить в виде html<br>");
                out.println("<input type=\"radio\" value=\"xml\" name=\"view\">Представить в виде xml<br>");
                out.println("<input type=\"submit\" value=\"Экспорт\">");
                out.println("</form></center>");
            } else {
                out.println("</table>");
                out.println("</form></center>");
            }
        }
        if (searchEmployee != null) {
            List<Employee> employees = employeeController.findByParameters(idEmployee, nameEmployee, surnameEmployee);
            out.println("<center><form action=\"ExportServlet\" target=\"_blank\" method=\"POST\"><table class=\"table table-bordered\"><tr><td>id</td><td>Имя</td><td>Фамилия</td></tr>");
            out.println("<input type=\"hidden\" id=\"idType\" name=\"type\" value=\"employees\">");//передача типа таблицы
            out.println("<input type=\"hidden\" id=\"idSizeEmployee\" name=\"sizeEmployee\" value=" + employees.size() + ">");//передача размера таблицы
            for (int i = 0; i < employees.size(); i++) {
                out.println("<tr>" +
                        "<td>" + employees.get(i).getId() + "<input type=\"hidden\" id=\"idIdEmployee" + i + "\" name=\"idEmployee" + i + "\" value=" + employees.get(i).getId() + ">" + "</td>" +
                        "<td>" + employees.get(i).getName() + "</td>" +
                        "<td>" + employees.get(i).getSurname() + "</td>" +
                        "</tr>");
            }
            if (employees.size() != 0) {
                out.println("</table>");
                out.println("<input type=\"radio\" value=\"up\" name=\"dependence\">Вверх по иерархии<br>");
                out.println("<input type=\"radio\" value=\"current\" name=\"dependence\" checked=\"checked\">Без зависимостей<br>");
                out.println("<input type=\"radio\" value=\"html\" name=\"view\" checked=\"checked\">Представить в виде html<br>");
                out.println("<input type=\"radio\" value=\"xml\" name=\"view\">Представить в виде xml<br>");
                out.println("<input type=\"submit\" value=\"Экспорт\">");
                out.println("</form></center>");
            } else {
                out.println("</table>");
                out.println("</form></center>");
            }
        }
    }
%>
<script>
    jQuery(function () {
        $("#idIdCompany").autocomplete("autocomplete/searchIdCompany.jsp", {delay: 100});
        $("#idNameCompany").autocomplete("autocomplete/searchNameCompany.jsp", {delay: 100});
        $("#idDescriptionCompany").autocomplete("autocomplete/searchDescriptionCompany.jsp", {delay: 100});
        $("#idIdDepartment").autocomplete("autocomplete/searchIdDepartment.jsp", {delay: 100});
        $("#idNameDepartment").autocomplete("autocomplete/searchNameDepartment.jsp", {delay: 100});
        $("#idDescriptionDepartment").autocomplete("autocomplete/searchDescriptionDepartment.jsp", {delay: 100});
        $("#idIdEmployee").autocomplete("autocomplete/searchIdEmployee.jsp", {delay: 100});
        $("#idNameEmployee").autocomplete("autocomplete/searchNameEmployee.jsp", {delay: 100});
        $("#idSurnameEmployee").autocomplete("autocomplete/searchSurnameEmployee.jsp", {delay: 100});
    });
</script>
</body>
</html>
