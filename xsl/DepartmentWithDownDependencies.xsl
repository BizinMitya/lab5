<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html" encoding="UTF-8"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css"/>
            </head>
            <body>
                <xsl:for-each select="departmentWithDownDependencies/department">
                    <H3>
                        <xsl:value-of select="concat('Company id: ', companies_id)"/>
                        <xsl:value-of select="concat('. Department id: ',id,'. ','Department name: ', name)"/>
                        <xsl:value-of select="concat('. Description: ', description)"/>
                        <xsl:value-of select="concat('. Department boss id: ', employees_boss)"/>
                    </H3>
                    <table class="table table-bordered">
                        <tr>
                            <td>
                                <strong>Employee ID</strong>
                            </td>
                            <td>
                                <strong>Employee name</strong>
                            </td>
                            <td>
                                <strong>Boss</strong>
                            </td>
                        </tr>
                        <xsl:for-each select="employee">
                            <tr>
                                <td>
                                    <xsl:value-of select="id"/>
                                </td>
                                <td>
                                    <xsl:value-of select="concat(name, ' ' , surname)"/>
                                </td>
                                <td>
                                    <xsl:value-of select="boss"/>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </table>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>