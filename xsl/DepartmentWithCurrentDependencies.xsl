<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"
                encoding="UTF-8"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css"/>
            </head>
            <body>
                <xsl:for-each select="departments/department">
                    <H4>
                        <xsl:value-of select="concat('Company id: ', companies_id)"/>
                    </H4>
                    <H4>
                        <xsl:value-of select="concat('Department id: ',id,'. ','Department name: ', name)"/>
                    </H4>
                    <H4>
                        <xsl:value-of select="concat('Description: ', description)"/>
                    </H4>
                    <H4>
                        <xsl:value-of select="concat('Department boss id: ', employees_boss)"/>
                    </H4>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>