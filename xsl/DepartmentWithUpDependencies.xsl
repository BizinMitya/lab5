<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"
                encoding="UTF-8"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css"/>
            </head>
            <body>
                <xsl:for-each select="departmentWithUpDependencies/company">
                    <H3>
                        <xsl:value-of select="concat('Company id: ',id,'. ','Company name: ', name)"/>
                        <xsl:value-of select="concat('. Company description: ', description)"/>
                    </H3>
                    <xsl:for-each select="department">
                        <H4>
                            <xsl:value-of select="concat('Department id: ',id,'. ','Department name: ', name)"/>
                            <xsl:value-of select="concat('. Department description: ', description)"/>
                            <xsl:value-of select="concat('. Department boss id: ', employees_boss)"/>
                        </H4>
                    </xsl:for-each>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>