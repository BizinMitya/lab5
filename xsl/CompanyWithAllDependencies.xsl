<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"
                encoding="UTF-8"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="bootstrap-3.3.6-dist/css/bootstrap.css"/>
            </head>
            <body>
                <xsl:for-each select="companyWithAllDependencies/company">
                    <H3>
                        <xsl:value-of
                                select="concat('Company id: ',id,'. ','Company name: ', name, '. Description: ', description)"/>
                    </H3>
                    <xsl:for-each select="department">
                        <H4>
                            <xsl:value-of
                                    select="concat('Department id: ',id,'. ','Department name: ', name, '. Description: ', description, '. Department boss id: ', employees_boss)"/>
                        </H4>
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <strong>Employee ID</strong>
                                </td>
                                <td>
                                    <strong>Employee name</strong>
                                </td>
                                <td>
                                    <strong>Boss</strong>
                                </td>
                            </tr>
                            <xsl:for-each select="employee">
                                <tr>
                                    <td>
                                        <xsl:value-of select="id"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="concat(name, ' ' , surname)"/>
                                    </td>
                                    <td>
                                        <xsl:value-of select="boss"/>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </table>
                    </xsl:for-each>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>