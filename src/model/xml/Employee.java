package model.xml;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

/**
 * Created by Dmitriy on 05.05.2016.
 */
@XmlElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee implements Serializable {
    private Integer id;
    private String name;
    private String surname;
    private Integer departments_id;
    private Integer boss;

    public Employee() {
    }

    public Employee(Integer id, String name, String surname, Integer departments_id, Integer boss) {
        this.setName(name);
        this.setSurname(surname);
        this.setId(id);
        this.setDepartments_id(departments_id);
        this.setBoss(boss);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDepartments_id() {
        return departments_id;
    }

    public void setDepartments_id(Integer departments_id) {
        this.departments_id = departments_id;
    }

    public Integer getBoss() {
        return boss;
    }

    public void setBoss(Integer boss) {
        this.boss = boss;
    }
}
