package model.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitriy on 17.04.2016.
 */
@XmlRootElement(name = "companies")
@XmlAccessorType(XmlAccessType.FIELD)
public class Companies implements Serializable {
    @XmlElement(name = "company")
    private List<Company> companies;

    public Companies(List<Company> companies) {
        this.setCompanies(companies);
    }

    public Companies() {

    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }
}
