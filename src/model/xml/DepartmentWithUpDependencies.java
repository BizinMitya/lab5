package model.xml;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;

/**
 * Created by Dmitriy on 17.04.2016.
 */
@XmlElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DepartmentWithUpDependencies implements Serializable {
    private Integer id;
    private String name;
    private String description;

    @javax.xml.bind.annotation.XmlElement(name = "department")
    private Department department;

    public DepartmentWithUpDependencies() {

    }

    public DepartmentWithUpDependencies(String name, String description, Integer id, Department department) {
        this.setName(name);
        this.setDescription(description);
        this.setId(id);
        this.setDepartment(department);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
