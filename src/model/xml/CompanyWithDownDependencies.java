package model.xml;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitriy on 17.04.2016.
 */
@XmlElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CompanyWithDownDependencies implements Serializable {
    private Integer id;
    private String name;
    private String description;

    @javax.xml.bind.annotation.XmlElement(name = "department")
    private List<DepartmentWithDownDependencies> departmentWithDownDependencies;

    public CompanyWithDownDependencies() {

    }

    public CompanyWithDownDependencies(String name, String description, Integer id, List<DepartmentWithDownDependencies> departmentWithDownDependencies) {
        this.setName(name);
        this.setDescription(description);
        this.setId(id);
        this.setDepartmentWithDownDependencies(departmentWithDownDependencies);
    }

    public List<DepartmentWithDownDependencies> getDepartmentWithDownDependencies() {
        return departmentWithDownDependencies;
    }

    public void setDepartmentWithDownDependencies(List<DepartmentWithDownDependencies> departmentWithDownDependencies) {
        this.departmentWithDownDependencies = departmentWithDownDependencies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
