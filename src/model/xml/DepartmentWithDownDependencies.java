package model.xml;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitriy on 17.04.2016.
 */
@XmlElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DepartmentWithDownDependencies implements Serializable {
    private Integer id;
    private String name;
    private String description;
    private Integer employees_boss;
    private Integer companies_id;

    @javax.xml.bind.annotation.XmlElement(name = "employee")
    private List<Employee> employees;

    public DepartmentWithDownDependencies() {

    }

    public DepartmentWithDownDependencies(String name, String description, Integer id, Integer employees_boss, Integer companies_id,
                                          List<Employee> employees) {
        this.setName(name);
        this.setDescription(description);
        this.setId(id);
        this.setEmployees_boss(employees_boss);
        this.setCompanies_id(companies_id);
        this.setEmployees(employees);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEmployees_boss() {
        return employees_boss;
    }

    public void setEmployees_boss(Integer employees_boss) {
        this.employees_boss = employees_boss;
    }

    public Integer getCompanies_id() {
        return companies_id;
    }

    public void setCompanies_id(Integer companies_id) {
        this.companies_id = companies_id;
    }
}
