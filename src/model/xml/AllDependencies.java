package model.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitriy on 17.04.2016.
 */
@XmlRootElement(name = "companyWithAllDependencies")
@XmlAccessorType(XmlAccessType.FIELD)
public class AllDependencies implements Serializable {
    @XmlElement(name = "company")
    private List<CompanyWithDownDependencies> companyWithDownDependencies;

    public AllDependencies() {

    }

    public AllDependencies(List<CompanyWithDownDependencies> companyWithDownDependencies) {
        this.setCompanyWithDownDependencies(companyWithDownDependencies);
    }

    public List<CompanyWithDownDependencies> getCompanyWithDownDependencies() {
        return companyWithDownDependencies;
    }

    public void setCompanyWithDownDependencies(List<CompanyWithDownDependencies> companyWithDownDependencies) {
        this.companyWithDownDependencies = companyWithDownDependencies;
    }
}
