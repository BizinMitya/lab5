package model.xml;

import java.io.Serializable;

/**
 * Created by Dmitriy on 21.04.2016.
 */
public class Statistics implements Serializable {
    private int numberOfCompanyInserted;
    private int numberOfDepartmentInserted;
    private int numberOfEmployeeInserted;
    private int numberOfCompanyUpdated;
    private int numberOfDepartmentUpdated;
    private int numberOfEmployeeUpdated;
    private int numberOfCompanyMissed;
    private int numberOfDepartmentMissed;
    private int numberOfEmployeeMissed;
    private int totalCompanies;
    private int totalDepartments;
    private int totalEmployees;

    public Statistics() {
        setNumberOfCompanyInserted(-1);
        setNumberOfCompanyUpdated(-1);
        setNumberOfDepartmentInserted(-1);
        setNumberOfDepartmentUpdated(-1);
        setNumberOfEmployeeInserted(-1);
        setNumberOfEmployeeUpdated(-1);
        setNumberOfCompanyMissed(-1);
        setNumberOfDepartmentMissed(-1);
        setNumberOfEmployeeMissed(-1);
        setTotalCompanies(-1);
        setTotalDepartments(-1);
        setTotalEmployees(-1);
    }

    public Statistics(int numberOfCompanyInserted, int numberOfDepartmentInserted, int numberOfEmployeeInserted,
                      int numberOfCompanyUpdated, int numberOfDepartmentUpdated, int numberOfEmployeeUpdated,
                      int numberOfCompanyMissed, int numberOfDepartmentMissed, int numberOfEmployeeMissed,
                      int totalCompanies, int totalDepartments, int totalEmployees) {
        this.setNumberOfCompanyInserted(numberOfCompanyInserted);
        this.setNumberOfCompanyUpdated(numberOfCompanyUpdated);
        this.setNumberOfDepartmentInserted(numberOfDepartmentInserted);
        this.setNumberOfDepartmentUpdated(numberOfDepartmentUpdated);
        this.setNumberOfEmployeeInserted(numberOfEmployeeInserted);
        this.setNumberOfEmployeeUpdated(numberOfEmployeeUpdated);
        this.setNumberOfCompanyMissed(numberOfCompanyMissed);
        this.setNumberOfDepartmentMissed(numberOfDepartmentMissed);
        this.setNumberOfEmployeeMissed(numberOfEmployeeMissed);
        this.setTotalCompanies(totalCompanies);
        this.setTotalDepartments(totalDepartments);
        this.setTotalEmployees(totalEmployees);
    }

    public int getNumberOfCompanyInserted() {
        return numberOfCompanyInserted;
    }

    public void setNumberOfCompanyInserted(int numberOfCompanyInserted) {
        this.numberOfCompanyInserted = numberOfCompanyInserted;
    }

    public int getNumberOfDepartmentInserted() {
        return numberOfDepartmentInserted;
    }

    public void setNumberOfDepartmentInserted(int numberOfDepartmentInserted) {
        this.numberOfDepartmentInserted = numberOfDepartmentInserted;
    }

    public int getNumberOfEmployeeInserted() {
        return numberOfEmployeeInserted;
    }

    public void setNumberOfEmployeeInserted(int numberOfEmployeeInserted) {
        this.numberOfEmployeeInserted = numberOfEmployeeInserted;
    }

    public int getNumberOfCompanyUpdated() {
        return numberOfCompanyUpdated;
    }

    public void setNumberOfCompanyUpdated(int numberOfCompanyUpdated) {
        this.numberOfCompanyUpdated = numberOfCompanyUpdated;
    }

    public int getNumberOfDepartmentUpdated() {
        return numberOfDepartmentUpdated;
    }

    public void setNumberOfDepartmentUpdated(int numberOfDepartmentUpdated) {
        this.numberOfDepartmentUpdated = numberOfDepartmentUpdated;
    }

    public int getNumberOfEmployeeUpdated() {
        return numberOfEmployeeUpdated;
    }

    public void setNumberOfEmployeeUpdated(int numberOfEmployeeUpdated) {
        this.numberOfEmployeeUpdated = numberOfEmployeeUpdated;
    }

    public int getTotalCompanies() {
        return totalCompanies;
    }

    public void setTotalCompanies(int totalCompanies) {
        this.totalCompanies = totalCompanies;
    }

    public int getTotalDepartments() {
        return totalDepartments;
    }

    public void setTotalDepartments(int totalDepartments) {
        this.totalDepartments = totalDepartments;
    }

    public int getTotalEmployees() {
        return totalEmployees;
    }

    public void setTotalEmployees(int totalEmployees) {
        this.totalEmployees = totalEmployees;
    }

    public int getNumberOfCompanyMissed() {
        return numberOfCompanyMissed;
    }

    public void setNumberOfCompanyMissed(int numberOfCompanyMissed) {
        this.numberOfCompanyMissed = numberOfCompanyMissed;
    }

    public int getNumberOfDepartmentMissed() {
        return numberOfDepartmentMissed;
    }

    public void setNumberOfDepartmentMissed(int numberOfDepartmentMissed) {
        this.numberOfDepartmentMissed = numberOfDepartmentMissed;
    }

    public int getNumberOfEmployeeMissed() {
        return numberOfEmployeeMissed;
    }

    public void setNumberOfEmployeeMissed(int numberOfEmployeeMissed) {
        this.numberOfEmployeeMissed = numberOfEmployeeMissed;
    }
}
