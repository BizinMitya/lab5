package model.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitriy on 17.04.2016.
 */
@XmlRootElement(name = "departmentWithUpDependencies")
@XmlAccessorType(XmlAccessType.FIELD)
public class UpDependencies implements Serializable {
    @XmlElement(name = "company")
    private List<DepartmentWithUpDependencies> departmentWithUpDependencies;

    public UpDependencies() {

    }

    public UpDependencies(List<DepartmentWithUpDependencies> departmentWithUpDependencies) {
        this.setDepartmentWithUpDependencies(departmentWithUpDependencies);
    }

    public List<DepartmentWithUpDependencies> getDepartmentWithUpDependencies() {
        return departmentWithUpDependencies;
    }

    public void setDepartmentWithUpDependencies(List<DepartmentWithUpDependencies> departmentWithUpDependencies) {
        this.departmentWithUpDependencies = departmentWithUpDependencies;
    }
}
