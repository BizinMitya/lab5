package model.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Dmitriy on 17.04.2016.
 */
@XmlRootElement(name = "departmentWithDownDependencies")
@XmlAccessorType(XmlAccessType.FIELD)
public class DownDependencies implements Serializable {
    @XmlElement(name = "department")
    private List<DepartmentWithDownDependencies> departmentWithDownDependencies;

    public DownDependencies() {

    }

    public DownDependencies(List<DepartmentWithDownDependencies> departmentWithDownDependencies) {
        this.setDepartmentWithDownDependencies(departmentWithDownDependencies);
    }

    public List<DepartmentWithDownDependencies> getDepartmentWithDownDependencies() {
        return departmentWithDownDependencies;
    }

    public void setDepartmentWithDownDependencies(List<DepartmentWithDownDependencies> departmentWithDownDependencies) {
        this.departmentWithDownDependencies = departmentWithDownDependencies;
    }
}
