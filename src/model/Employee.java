package model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Dmitriy on 10.02.2016.
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "employees")
public class Employee implements Serializable {
    @Column(nullable = false)
    @Id
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @ManyToOne
    @JoinColumn(name = "departments_id", nullable = false)
    private Department department;//департамент сотрудника

    @ManyToOne
    @JoinColumn(name = "boss", nullable = true)
    private Employee employee;//босс у сотрудника

    @OneToOne(mappedBy = "employee")//вот из-за этого не работало свиное ухо,брался department вместо department1
    private Department department1;//департамент для связи босса и департамента

    @OneToMany(mappedBy = "employees", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Employee> employees = new HashSet<>(0);

    public Employee() {
    }

    public Employee(Integer id, String name, String surname, Department department, Employee employee) {
        this.setName(name);
        this.setSurname(surname);
        this.setId(id);
        this.setDepartment(department);
        this.setEmployee(employee);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public Department getDepartment1() {
        return department1;
    }

    public void setDepartment1(Department department1) {
        this.department1 = department1;
    }
}
