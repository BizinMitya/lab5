package model;

import controllers.impl.CompanyControllerImpl;
import controllers.impl.DepartmentControllerImpl;

/**
 * Created by Dmitriy on 10.02.2016.
 */
public class Models {

    public static StringBuffer drawPath(Object object) {
        StringBuffer stringBuffer = new StringBuffer();
        if (object instanceof Company) {
            stringBuffer.append("<a href=\"companies.jsp?id=").append(((Company) object).getId()).append("\">").
                    append(((Company) object).getName()).append("</a>");
        }
        if (object instanceof Department) {
            CompanyControllerImpl companyController = new CompanyControllerImpl();
            stringBuffer.append("<a href=\"companies.jsp?id=").append(((Department) object).getCompany().getId()).append("\">").
                    append(companyController.findOne(((Department) object).getCompany().getId()).
                    getName()).append("</a>");
            stringBuffer.append(" -> ");
            stringBuffer.append("<a href=\"departments.jsp?id=").append(((Department) object).getId()).append("\">").
                    append(((Department) object).getName()).append("</a>");
        }
        if (object instanceof Employee) {
            CompanyControllerImpl companyController = new CompanyControllerImpl();
            DepartmentControllerImpl departmentController = new DepartmentControllerImpl();
            stringBuffer.append("<a href=\"companies.jsp?id=").
                    append(departmentController.findOne(((Employee) object).getDepartment().getId()).getCompany().getId()).append("\">").
                    append(companyController.findOne(departmentController.findOne(((Employee) object).getDepartment().getId()).getCompany().getId()).getName()).
                    append("</a>");
            stringBuffer.append(" -> ");
            stringBuffer.append("<a href=\"departments.jsp?id=").append(((Employee) object).getDepartment().getId()).append("\">").
                    append(departmentController.findOne(((Employee) object).getDepartment().getId()).getName()).append("</a>");
            stringBuffer.append(" -> ");
            stringBuffer.append("<a href=\"employees.jsp?id=").append(((Employee) object).getId()).append("\">").
                    append(((Employee) object).getName()).append(" ").append(((Employee) object).getSurname()).append("</a>");
        }
        return stringBuffer;
    }
}
