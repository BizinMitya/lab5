package controllers;

import model.Employee;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 27.02.2016.
 */
public interface EmployeeController {

    void helper(int parent, List<Employee> employees, int idDepartment);

    List<Employee> findByParameters(String id, String name, String surname);

    List<Employee> findAll();

    Employee findOne(Integer id);

    void insert(Employee employee) throws NamingException, SQLException;

    void delete(int id) throws NamingException, SQLException;

    void update(Employee employee) throws NamingException, SQLException;

    List<Employee> findByIds(List<Integer> ids);

    List<Employee> findByDepartmentId(Integer departmentId);

    List<String> findByLikeParameter(String parameter, String like);

    StringBuffer buildTree(List<Employee> employees, int parent, StringBuffer stringBuffer);

    List<Employee> getResult();
}
