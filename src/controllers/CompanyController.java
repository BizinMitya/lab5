package controllers;

import model.Company;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 31.01.2016.
 */
public interface CompanyController {

    List<Company> findByParameters(String id, String name, String description);

    void insert(Company company) throws NamingException, SQLException;

    void delete(int id) throws NamingException, SQLException;

    void update(Company company) throws NamingException, SQLException;

    List<Company> findAll();

    Company findOne(Integer id);

    List<Company> findByIds(List<Integer> ids);

    List<String> findByLikeParameter(String parameter, String like);

}
