package controllers.servlet;

import controllers.converter.Converter;
import controllers.converter.ConverterHome;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.ejb.CreateException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Dmitriy on 16.04.2016.
 */
@WebServlet(name = "ExportServlet")
public class ExportServlet extends HttpServlet {
    private static Document document;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try (OutputStream out = response.getOutputStream()) {
            Converter converter = (Converter) request.getSession().getAttribute("converter");
            if (converter == null) {
                InitialContext initialContext = new InitialContext();
                ConverterHome home = (ConverterHome) initialContext.lookup("ejb:Lab5_ear_exploded/ejb/ConverterBean!controllers.converter.ConverterHome");
                converter = home.create();
                request.getSession().setAttribute("converter", converter);
            }
            String type = request.getParameter("type");
            int size = 0;
            String parameter = "";
            switch (type) {
                case "companies": {
                    size = Integer.parseInt(request.getParameter("sizeCompany"));
                    parameter = "idCompany";
                    break;
                }
                case "departments": {
                    size = Integer.parseInt(request.getParameter("sizeDepartment"));
                    parameter = "idDepartment";
                    break;
                }
                case "employees": {
                    size = Integer.parseInt(request.getParameter("sizeEmployee"));
                    parameter = "idEmployee";
                    break;
                }
            }
            List<Integer> ids = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                ids.add(Integer.parseInt(request.getParameter(parameter + i)));
            }
            String dependence = request.getParameter("dependence");
            String view = request.getParameter("view");
            File file = null;
            if (view.equals("html")) {
                file = converter.exportDb(type, ids, dependence, view);
                response.setContentType("text/html");
                File stylesheet = null;
                switch (type) {
                    case "companies": {
                        if (dependence.equals("down"))
                            stylesheet = new File("xml/xsl/CompanyWithAllDependencies.xsl");
                        if (dependence.equals("current"))
                            stylesheet = new File("xml/xsl/CompanyWithCurrentDependencies.xsl");
                        break;
                    }
                    case "departments": {
                        if (dependence.equals("down"))
                            stylesheet = new File("xml/xsl/DepartmentWithDownDependencies.xsl");
                        if (dependence.equals("current"))
                            stylesheet = new File("xml/xsl/DepartmentWithCurrentDependencies.xsl");
                        if (dependence.equals("all"))
                            stylesheet = new File("xml/xsl/CompanyWithAllDependencies.xsl");
                        if (dependence.equals("up"))
                            stylesheet = new File("xml/xsl/DepartmentWithUpDependencies.xsl");
                        break;
                    }
                    case "employees": {
                        if (dependence.equals("up"))
                            stylesheet = new File("xml/xsl/CompanyWithAllDependencies.xsl");
                        if (dependence.equals("current"))
                            stylesheet = new File("xml/xsl/EmployeeWithCurrentDependencies.xsl");
                        break;
                    }
                }
                Random random = new Random();
                int html = random.nextInt();
                FileWriter res = new FileWriter("xml/html/" + html + ".html");
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                document = builder.parse(file);
                TransformerFactory tFactory = TransformerFactory.newInstance();
                StreamSource stylesource = new StreamSource(stylesheet);
                Transformer transformer = tFactory.newTransformer(stylesource);
                DOMSource source = new DOMSource(document);
                StreamResult result = new StreamResult(res);
                transformer.transform(source, result);
                res.flush();
                res.close();
                FileInputStream in1 = new FileInputStream("xml/html/" + html + ".html");
                byte[] buffer = new byte[4096];
                int length;
                while ((length = in1.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
                in1.close();
                out.flush();
                File fileHtml = new File("xml/html/" + html + ".html");
                fileHtml.delete();
            }
            if (view.equals("xml")) {
                file = converter.exportDb(type, ids, dependence, view);
                FileInputStream in = new FileInputStream(file);
                response.setContentType("text/xml");
                byte[] buffer = new byte[4096];
                int length;
                while ((length = in.read(buffer)) > 0) {
                    out.write(buffer, 0, length);
                }
                in.close();
                out.flush();
            }
            file.delete();
        } catch (CreateException | NamingException | ParserConfigurationException | SAXException | TransformerException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
