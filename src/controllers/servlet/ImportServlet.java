package controllers.servlet;

import controllers.converter.Converter;
import controllers.converter.ConverterHome;
import model.xml.Statistics;

import javax.ejb.CreateException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.bind.JAXBException;
import java.io.*;
import java.sql.SQLException;
import java.util.Random;

/**
 * Created by Dmitriy on 17.04.2016.
 */

@WebServlet(name = "ImportServlet")
@MultipartConfig
public class ImportServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Random random = new Random();
        try {
            Converter converter = (Converter) request.getSession().getAttribute("converter");
            if (converter == null) {
                InitialContext initialContext = new InitialContext();
                ConverterHome home = (ConverterHome) initialContext.lookup("java:global/Lab5_ear_exploded/ejb/ConverterBean!controllers.converter.ConverterHome");
                converter = home.create();
                request.getSession().setAttribute("converter", converter);
            }
            Part filePart = request.getPart("file");
            String duplicate = request.getParameter("duplicate");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(filePart.getInputStream()));
            String xmlString = "";
            StringBuffer stringBuffer = new StringBuffer();
            int r = random.nextInt();
            FileWriter fileWriter = new FileWriter("xml\\" + r + ".xml");
            while ((xmlString = bufferedReader.readLine()) != null) {
                stringBuffer.append(xmlString);
            }
            bufferedReader.close();
            fileWriter.write(String.valueOf(stringBuffer));
            fileWriter.close();
            File file = new File("xml\\" + r + ".xml");
            response.setContentType("text/html");
            response.setCharacterEncoding("UTF-8");
            try {
                Statistics statistics = converter.importDb(file, duplicate);
                response.getWriter().print(String.valueOf(getStringOfStatistics(statistics)));
            } catch (JAXBException e) {
                response.getWriter().print("<h1>Импортируемый файл не выбран или не соответствует схеме!<h1>");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            file.delete();
        } catch (CreateException | NamingException e) {
            e.printStackTrace();
        }
    }

    private String getStringOfStatistics(Statistics statistics) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<h1>Импорт выполнен успешно!</h1><br>");

        if (statistics.getTotalCompanies() != -1) {
            stringBuffer.append("Всего компаний в файле: ").append(statistics.getTotalCompanies()).append("<br>");
        }
        if (statistics.getTotalDepartments() != -1) {
            stringBuffer.append("Всего департаментов в файле: ").append(statistics.getTotalDepartments()).append("<br>");
        }
        if (statistics.getTotalEmployees() != -1) {
            stringBuffer.append("Всего сотрудников в файле: ").append(statistics.getTotalEmployees()).append("<br>");
        }
        if (statistics.getNumberOfCompanyInserted() != -1) {
            stringBuffer.append("Компаний добавлено: ").append(statistics.getNumberOfCompanyInserted()).append("<br>");
        }
        if (statistics.getNumberOfDepartmentInserted() != -1) {
            stringBuffer.append("Департаментов добавлено: ").append(statistics.getNumberOfDepartmentInserted()).append("<br>");
        }
        if (statistics.getNumberOfEmployeeInserted() != -1) {
            stringBuffer.append("Сотрудников добавлено: ").append(statistics.getNumberOfEmployeeInserted()).append("<br>");
        }

        if (statistics.getNumberOfCompanyUpdated() != -1) {
            stringBuffer.append("Компаний перезаписано: ").append(statistics.getNumberOfCompanyUpdated()).append("<br>");
        }
        if (statistics.getNumberOfDepartmentUpdated() != -1) {
            stringBuffer.append("Департаментов перезаписано: ").append(statistics.getNumberOfDepartmentUpdated()).append("<br>");
        }
        if (statistics.getNumberOfEmployeeUpdated() != -1) {
            stringBuffer.append("Сотрудников перезаписано: ").append(statistics.getNumberOfEmployeeUpdated()).append("<br>");
        }

        if (statistics.getNumberOfCompanyMissed() != -1) {
            stringBuffer.append("Компаний пропущено: ").append(statistics.getNumberOfCompanyMissed()).append("<br>");
        }
        if (statistics.getNumberOfDepartmentMissed() != -1) {
            stringBuffer.append("Департаментов пропущено: ").append(statistics.getNumberOfDepartmentMissed()).append("<br>");
        }
        if (statistics.getNumberOfEmployeeMissed() != -1) {
            stringBuffer.append("Сотрудников пропущено: ").append(statistics.getNumberOfEmployeeMissed()).append("<br>");
        }
        return String.valueOf(stringBuffer);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
