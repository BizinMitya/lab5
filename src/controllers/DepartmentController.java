package controllers;

import model.Department;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 24.02.2016.
 */
public interface DepartmentController {

    List<Department> findByParameters(String id, String name, String description);

    List<Department> findAll();

    Department findOne(Integer id);

    void insert(Department department) throws NamingException, SQLException;

    void delete(int id) throws NamingException, SQLException;

    void update(Department department) throws NamingException, SQLException;

    List<Department> findByIds(List<Integer> ids);

    List<Department> findByCompanyId(Integer companyId);

    List<String> findByLikeParameter(String parameter, String like);
}
