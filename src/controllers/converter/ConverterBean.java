package controllers.converter;

import controllers.CompanyController;
import controllers.DepartmentController;
import controllers.EmployeeController;
import controllers.impl.CompanyControllerImpl;
import controllers.impl.DepartmentControllerImpl;
import controllers.impl.EmployeeControllerImpl;
import model.Company;
import model.Department;
import model.Employee;
import model.xml.*;

import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.naming.NamingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by Dmitriy on 16.04.2016.
 */
public class ConverterBean implements SessionBean, Serializable {

    public ConverterBean() {

    }

    public model.xml.Company toXmlCompany(Company company) {
        return new model.xml.Company(company.getId(), company.getName(), company.getDescription());
    }

    public Company toDbCompany(model.xml.Company company) {
        return new Company(company.getId(), company.getName(), company.getDescription());
    }

    public model.xml.Department toXmlDepartment(Department department) {
        model.xml.Department department1 = new model.xml.Department();
        department1.setId(department.getId());
        department1.setName(department.getName());
        department1.setDescription(department.getDescription());
        department1.setCompanies_id(department.getCompany().getId());
        if (department.getEmployee() == null) {
            department1.setEmployees_boss(0);
        } else {
            department1.setEmployees_boss(department.getEmployee().getId());
        }
        return department1;
    }

    public Department toDbDepartment(model.xml.Department department) {
        Department department1 = new Department();
        department1.setId(department.getId());
        department1.setName(department.getName());
        department1.setDescription(department.getDescription());
        Company company = new Company();
        company.setId(department.getCompanies_id());
        department1.setCompany(company);
        if (department.getEmployees_boss() != 0) {
            Employee employee = new Employee();
            employee.setId(department.getEmployees_boss());
            department1.setEmployee(employee);
        } else {
            department1.setEmployee(null);
        }
        return department1;
    }

    public model.xml.Employee toXmlEmployee(Employee employee) {
        model.xml.Employee employee1 = new model.xml.Employee();
        employee1.setId(employee.getId());
        employee1.setName(employee.getName());
        employee1.setSurname(employee.getSurname());
        if (employee.getEmployee() == null) {
            employee1.setBoss(0);
        } else {
            employee1.setBoss(employee.getEmployee().getId());
        }
        employee1.setDepartments_id(employee.getDepartment().getId());
        return employee1;
    }

    public Employee toDbEmployee(model.xml.Employee employee) {
        Employee employee1 = new Employee();
        employee1.setId(employee.getId());
        employee1.setName(employee.getName());
        employee1.setSurname(employee.getSurname());
        Department department = new Department();
        department.setId(employee.getDepartments_id());
        employee1.setDepartment(department);
        if (employee.getBoss() != 0) {
            Employee employee2 = new Employee();
            employee2.setId(employee.getBoss());
            employee1.setEmployee(employee2);
        } else {
            employee1.setEmployee(null);
        }
        return employee1;
    }

    public List<model.xml.Company> toXmlListCompany(List<Company> companies) {
        List<model.xml.Company> xmlListCompany = new ArrayList<>();
        for (Company company : companies) {
            xmlListCompany.add(toXmlCompany(company));
        }
        return xmlListCompany;
    }

    public List<Company> toDbListCompany(List<model.xml.Company> companies) {
        List<Company> dbListCompany = new ArrayList<>();
        for (model.xml.Company company : companies) {
            dbListCompany.add(toDbCompany(company));
        }
        return dbListCompany;
    }

    public List<model.xml.Department> toXmlListDepartment(List<Department> departments) {
        List<model.xml.Department> xmlListDepartment = new ArrayList<>();
        for (Department department : departments) {
            xmlListDepartment.add(toXmlDepartment(department));
        }
        return xmlListDepartment;
    }

    public List<Department> toDbListDepartment(List<model.xml.Department> departments) {
        List<Department> dbListDepartment = new ArrayList<>();
        for (model.xml.Department department : departments) {
            dbListDepartment.add(toDbDepartment(department));
        }
        return dbListDepartment;
    }

    public List<model.xml.Employee> toXmlListEmployee(List<Employee> employees) {
        List<model.xml.Employee> xmlListEmployee = new ArrayList<>();
        for (Employee employee : employees) {
            xmlListEmployee.add(toXmlEmployee(employee));
        }
        return xmlListEmployee;
    }

    public List<Employee> toDbListEmployee(List<model.xml.Employee> employees) {
        List<Employee> dbListEmployee = new ArrayList<>();
        for (model.xml.Employee employee : employees) {
            dbListEmployee.add(toDbEmployee(employee));
        }
        return dbListEmployee;
    }

    public File exportDb(String type, List<Integer> ids, String dependence, String view) {
        CompanyController companyController = new CompanyControllerImpl();
        DepartmentController departmentController = new DepartmentControllerImpl();
        EmployeeController employeeController = new EmployeeControllerImpl();
        File file = null;
        Random random = new Random();
        if (type.equals("companies")) {
            if (dependence.equals("current")) {
                try {
                    file = new File("xml\\" + random.nextInt() + ".xml");
                    JAXBContext jc = JAXBContext.newInstance(Companies.class);
                    Marshaller m = jc.createMarshaller();
                    Companies companies = new Companies(toXmlListCompany(companyController.findByIds(ids)));
                    m.setProperty("jaxb.formatted.output", Boolean.TRUE);//отступы
                    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    if (view.equals("html")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE companies SYSTEM \"dtd/CompanyWithCurrentDependencies.dtd\">\n<?xml-stylesheet type=\"text/xsl\" href=\"xsl/CompanyWithCurrentDependencies.xsl\"?>\n");
                    }
                    if (view.equals("xml")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE companies SYSTEM \"dtd/CompanyWithCurrentDependencies.dtd\">");
                    }
                    m.marshal(companies, file);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
            if (dependence.equals("down")) {
                try {
                    file = new File("xml\\" + random.nextInt() + ".xml");
                    JAXBContext jc = JAXBContext.newInstance(AllDependencies.class);
                    Marshaller m = jc.createMarshaller();
                    List<CompanyWithDownDependencies> companyWithDownDependencies = new ArrayList<>();
                    for (int i = 0; i < ids.size(); i++) {
                        Company company = companyController.findOne(ids.get(i));
                        List<DepartmentWithDownDependencies> departmentWithDownDependencies = new ArrayList<>();
                        List<Department> departments = departmentController.findByCompanyId(company.getId());
                        for (int j = 0; j < departments.size(); j++) {
                            Department department = departments.get(j);
                            List<Employee> employees = employeeController.findByDepartmentId(departments.get(j).getId());
                            departmentWithDownDependencies.add(new DepartmentWithDownDependencies(department.getName(),
                                    department.getDescription(), department.getId(), department.getEmployee() == null ? 0 : department.getEmployee().getId(),
                                    department.getCompany().getId(), toXmlListEmployee(employees)));
                        }
                        companyWithDownDependencies.add(new CompanyWithDownDependencies(company.getName(), company.getDescription(),
                                company.getId(), departmentWithDownDependencies));
                    }
                    m.setProperty("jaxb.formatted.output", Boolean.TRUE);//отступы
                    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    if (view.equals("html")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE all SYSTEM \"dtd/CompanyWithAllDependencies.dtd\">\n<?xml-stylesheet type=\"text/xsl\" href=\"xsl/CompanyWithAllDependencies.xsl\"?>\n");
                    }
                    if (view.equals("xml")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE all SYSTEM \"dtd/CompanyWithAllDependencies.dtd\">");
                    }
                    m.marshal(new AllDependencies(companyWithDownDependencies), file);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        }
        if (type.equals("departments")) {
            if (dependence.equals("current")) {
                try {
                    file = new File("xml\\" + random.nextInt() + ".xml");
                    JAXBContext jc = JAXBContext.newInstance(Departments.class);
                    Marshaller m = jc.createMarshaller();
                    Departments departments = new Departments(toXmlListDepartment(departmentController.findByIds(ids)));
                    m.setProperty("jaxb.formatted.output", Boolean.TRUE);//отступы
                    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    if (view.equals("html")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE departments SYSTEM \"dtd/DepartmentWithCurrentDependencies.dtd\">\n<?xml-stylesheet type=\"text/xsl\" href=\"xsl/DepartmentWithCurrentDependencies.xsl\"?>\n");
                    }
                    if (view.equals("xml")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE departments SYSTEM \"dtd/DepartmentWithCurrentDependencies.dtd\">");
                    }
                    m.marshal(departments, file);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
            if (dependence.equals("all")) {
                try {
                    file = new File("xml\\" + random.nextInt() + ".xml");
                    JAXBContext jc = JAXBContext.newInstance(AllDependencies.class);
                    Marshaller m = jc.createMarshaller();
                    List<Department> departments = departmentController.findByIds(ids);
                    List<CompanyWithDownDependencies> companyWithDownDependencies = new ArrayList<>();
                    for (int i = 0; i < departments.size(); i++) {
                        Company company = companyController.findOne(departments.get(i).getCompany().getId());
                        Department department = departments.get(i);
                        List<Employee> employees = employeeController.findByDepartmentId(departments.get(i).getId());
                        List<DepartmentWithDownDependencies> departmentWithDownDependencies = new ArrayList<>();
                        departmentWithDownDependencies.add(new DepartmentWithDownDependencies(department.getName(),
                                department.getDescription(), department.getId(), department.getEmployee() == null ? 0 : department.getEmployee().getId(),
                                department.getCompany().getId(), toXmlListEmployee(employees)));
                        companyWithDownDependencies.add(new CompanyWithDownDependencies(company.getName(), company.getDescription(), company.getId(),
                                departmentWithDownDependencies));
                    }
                    m.setProperty("jaxb.formatted.output", Boolean.TRUE);//отступы
                    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    if (view.equals("html")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE all SYSTEM \"dtd/DepartmentWithAllDependencies.dtd\">\n<?xml-stylesheet type=\"text/xsl\" href=\"xsl/CompanyWithAllDependencies.xsl\"?>\n");
                    }
                    if (view.equals("xml")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE all SYSTEM \"dtd/DepartmentWithAllDependencies.dtd\">");
                    }
                    m.marshal(new AllDependencies(companyWithDownDependencies), file);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
            if (dependence.equals("up")) {
                try {
                    file = new File("xml\\" + random.nextInt() + ".xml");
                    JAXBContext jc = JAXBContext.newInstance(UpDependencies.class);
                    Marshaller m = jc.createMarshaller();
                    List<DepartmentWithUpDependencies> departmentWithUpDependencies = new ArrayList<>();
                    List<Department> departments = departmentController.findByIds(ids);
                    for (int i = 0; i < departments.size(); i++) {
                        Department department = departments.get(i);
                        Company company = companyController.findOne(department.getCompany().getId());
                        departmentWithUpDependencies.add(new DepartmentWithUpDependencies(company.getName(),
                                company.getDescription(), company.getId(), toXmlDepartment(department)));
                    }
                    m.setProperty("jaxb.formatted.output", Boolean.TRUE);//отступы
                    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    if (view.equals("html")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE departmentWithUpDependencies SYSTEM \"dtd/DepartmentWithUpDependencies.dtd\">\n<?xml-stylesheet type=\"text/xsl\" href=\"xsl/DepartmentWithUpDependencies.xsl\"?>\n");
                    }
                    if (view.equals("xml")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE departmentWithUpDependencies SYSTEM \"dtd/DepartmentWithUpDependencies.dtd\">");
                    }
                    m.marshal(new UpDependencies(departmentWithUpDependencies), file);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
            if (dependence.equals("down")) {
                try {
                    file = new File("xml\\" + random.nextInt() + ".xml");
                    JAXBContext jc = JAXBContext.newInstance(DownDependencies.class);
                    Marshaller m = jc.createMarshaller();
                    List<DepartmentWithDownDependencies> departmentWithDownDependencies = new ArrayList<>();
                    List<Department> departments = departmentController.findByIds(ids);
                    for (int i = 0; i < departments.size(); i++) {
                        Department department = departments.get(i);
                        List<Employee> employees = employeeController.findByDepartmentId(department.getId());
                        departmentWithDownDependencies.add(new DepartmentWithDownDependencies(department.getName(),
                                department.getDescription(), department.getId(), department.getEmployee() == null ? 0 : department.getEmployee().getId(),
                                department.getCompany().getId(), toXmlListEmployee(employees)));
                    }
                    m.setProperty("jaxb.formatted.output", Boolean.TRUE);//отступы
                    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    if (view.equals("html")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE departmentWithDownDependencies SYSTEM \"dtd/DepartmentWithDownDependencies.dtd\">\n<?xml-stylesheet type=\"text/xsl\" href=\"xsl/DepartmentWithDownDependencies.xsl\"?>\n");
                    }
                    if (view.equals("xml")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE departmentWithDownDependencies SYSTEM \"dtd/DepartmentWithDownDependencies.dtd\">");
                    }
                    m.marshal(new DownDependencies(departmentWithDownDependencies), file);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        }
        if (type.equals("employees")) {
            if (dependence.equals("current")) {
                try {
                    file = new File("xml\\" + random.nextInt() + ".xml");
                    JAXBContext jc = JAXBContext.newInstance(Employees.class);
                    Marshaller m = jc.createMarshaller();
                    Employees employees = new Employees(toXmlListEmployee(employeeController.findByIds(ids)));
                    m.setProperty("jaxb.formatted.output", Boolean.TRUE);//отступы
                    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    if (view.equals("html")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE employees SYSTEM \"dtd/EmployeeWithCurrentDependencies.dtd\">\n<?xml-stylesheet type=\"text/xsl\" href=\"xsl/EmployeeWithCurrentDependencies.xsl\"?>\n");
                    }
                    if (view.equals("xml")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE employees SYSTEM \"dtd/EmployeeWithCurrentDependencies.dtd\">");
                    }
                    m.marshal(employees, file);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
            if (dependence.equals("up")) {
                try {
                    file = new File("xml\\" + random.nextInt() + ".xml");
                    JAXBContext jc = JAXBContext.newInstance(AllDependencies.class);
                    Marshaller m = jc.createMarshaller();
                    List<Employee> employees = employeeController.findByIds(ids);
                    List<CompanyWithDownDependencies> companyWithDownDependencies = new ArrayList<>();
                    for (int i = 0; i < employees.size(); i++) {
                        Employee employee = employees.get(i);
                        Department department = departmentController.findOne(employees.get(i).getDepartment().getId());
                        Company company = companyController.findOne(department.getCompany().getId());
                        List<DepartmentWithDownDependencies> departmentWithDownDependencies = new LinkedList<>();
                        List<Employee> employees1 = new ArrayList<>();
                        employees1.add(employee);
                        departmentWithDownDependencies.add(new DepartmentWithDownDependencies(department.getName(),
                                department.getDescription(), department.getId(), department.getEmployee() == null ? 0 : department.getEmployee().getId(),
                                department.getCompany().getId(), toXmlListEmployee(employees1)));
                        companyWithDownDependencies.add(new CompanyWithDownDependencies(company.getName(), company.getDescription(),
                                company.getId(), departmentWithDownDependencies));
                    }
                    m.setProperty("jaxb.formatted.output", Boolean.TRUE);//отступы
                    m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);
                    if (view.equals("html")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE all SYSTEM \"dtd/EmployeeWithAllDependencies.dtd\">\n<?xml-stylesheet type=\"text/xsl\" href=\"xsl/CompanyWithAllDependencies.xsl\"?>\n");
                    }
                    if (view.equals("xml")) {
                        m.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE all SYSTEM \"dtd/EmployeeWithAllDependencies.dtd\">");
                    }
                    m.marshal(new AllDependencies(companyWithDownDependencies), file);
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        }
        return file;
    }

    public Statistics importDb(File file, String duplicate) throws JAXBException, SQLException, NamingException {
        Statistics statistics = new Statistics();
        JAXBContext jaxbContext = JAXBContext.newInstance(Companies.class, AllDependencies.class, Departments.class,
                UpDependencies.class, DownDependencies.class, Employees.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Object object = unmarshaller.unmarshal(file);
        CompanyController companyController = new CompanyControllerImpl();
        DepartmentController departmentController = new DepartmentControllerImpl();
        EmployeeController employeeController = new EmployeeControllerImpl();
        if (object instanceof Companies) {
            List<Company> companies = toDbListCompany(((Companies) object).getCompanies());
            if (duplicate.equals("rewrite")) {
                int numberOfUpdated = 0;
                int numberOfInserted = 0;
                for (Company company : companies) {
                    if (companyController.findOne(company.getId()) == null) {
                        companyController.insert(company);
                        numberOfInserted++;
                    } else {
                        companyController.delete(company.getId());
                        companyController.insert(company);
                        numberOfUpdated++;
                    }
                }
                statistics.setTotalCompanies(companies.size());
                statistics.setNumberOfCompanyInserted(numberOfInserted);
                statistics.setNumberOfCompanyUpdated(numberOfUpdated);
            }
            if (duplicate.equals("miss")) {
                int numberOfMissed = 0;
                int numberOfInserted = 0;
                for (Company company : companies) {
                    if (companyController.findOne(company.getId()) == null) {
                        companyController.insert(company);
                        numberOfInserted++;
                    } else {
                        numberOfMissed++;
                    }
                }
                statistics.setTotalCompanies(companies.size());
                statistics.setNumberOfCompanyInserted(numberOfInserted);
                statistics.setNumberOfCompanyMissed(numberOfMissed);
            }
        }
        if (object instanceof AllDependencies) {
            List<CompanyWithDownDependencies> companyWithDownDependenciesList = ((AllDependencies) object).getCompanyWithDownDependencies();
            if (duplicate.equals("rewrite")) {
                List<Department> listFormerDepartments = new ArrayList<>();
                List<Employee> listFormerEmployees = new ArrayList<>();
                int numberOfUpdatedDepartments = 0;
                int numberOfInsertedDepartments = 0;
                int numberOfEmployees = 0;
                int numberOfDepartments = 0;
                int numberOfUpdatedEmployees = 0;
                int numberOfInsertedEmployees = 0;
                int numberOfUpdatedCompanies = 0;
                int numberOfInsertedCompanies = 0;
                //пока добавление без боссов
                for (CompanyWithDownDependencies companyWithDownDependencies : companyWithDownDependenciesList) {
                    Company company = new Company(companyWithDownDependencies.getId(), companyWithDownDependencies.getName(),
                            companyWithDownDependencies.getDescription());
                    if (companyController.findOne(company.getId()) == null) {
                        companyController.insert(company);
                        numberOfInsertedCompanies++;
                    } else {
                        listFormerDepartments = departmentController.findByCompanyId(company.getId());
                        listFormerEmployees = new ArrayList<>();
                        for (Department department1 : listFormerDepartments) {
                            List<Employee> employees = employeeController.findByDepartmentId(department1.getId());
                            for (Employee employee : employees) {
                                listFormerEmployees.add(employee);
                            }
                        }
                        companyController.delete(company.getId());
                        companyController.insert(company);
                        numberOfUpdatedCompanies++;
                    }
                    List<DepartmentWithDownDependencies> departmentWithDownDependenciesList = companyWithDownDependencies.getDepartmentWithDownDependencies();
                    numberOfDepartments += departmentWithDownDependenciesList.size();
                    for (DepartmentWithDownDependencies departmentWithDownDependencies : departmentWithDownDependenciesList) {
                        Department department = new Department(departmentWithDownDependencies.getId(),
                                departmentWithDownDependencies.getName(), departmentWithDownDependencies.getDescription(),
                                company, null);
                        if (departmentController.findOne(department.getId()) == null) {
                            boolean flag = false;
                            departmentController.insert(department);
                            for (Department department1 : listFormerDepartments) {
                                if (department1.getId().equals(department.getId())) {
                                    numberOfUpdatedDepartments++;
                                    flag = true;
                                    break;
                                }
                            }
                            if (!flag) {
                                numberOfInsertedDepartments++;
                            }
                        } else {
                            departmentController.delete(department.getId());
                            departmentController.insert(department);
                            numberOfUpdatedDepartments++;
                        }
                        List<Employee> employees = toDbListEmployee(departmentWithDownDependencies.getEmployees());
                        numberOfEmployees += employees.size();
                        for (Employee employee : employees) {
                            Employee employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(), department, null);
                            if (employeeController.findOne(employee.getId()) == null) {
                                boolean flag = false;
                                employeeController.insert(employee1);
                                for (Employee employee2 : listFormerEmployees) {
                                    if (employee2.getId().equals(employee1.getId())) {
                                        numberOfUpdatedEmployees++;
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag) {
                                    numberOfInsertedEmployees++;
                                }
                            } else {
                                employeeController.delete(employee1.getId());
                                employeeController.insert(employee1);
                                numberOfUpdatedEmployees++;
                            }
                        }
                    }
                }
                statistics.setTotalCompanies(companyWithDownDependenciesList.size());
                statistics.setTotalDepartments(numberOfDepartments);
                statistics.setTotalEmployees(numberOfEmployees);
                statistics.setNumberOfCompanyInserted(numberOfInsertedCompanies);
                statistics.setNumberOfDepartmentInserted(numberOfInsertedDepartments);
                statistics.setNumberOfEmployeeInserted(numberOfInsertedEmployees);
                statistics.setNumberOfCompanyUpdated(numberOfUpdatedCompanies);
                statistics.setNumberOfDepartmentUpdated(numberOfUpdatedDepartments);
                statistics.setNumberOfEmployeeUpdated(numberOfUpdatedEmployees);
            }
            if (duplicate.equals("miss")) {
                int numberOfMissedDepartments = 0;
                int numberOfInsertedDepartments = 0;
                int numberOfEmployees = 0;
                int numberOfDepartments = 0;
                int numberOfMissedEmployees = 0;
                int numberOfInsertedEmployees = 0;
                int numberOfMissedCompanies = 0;
                int numberOfInsertedCompanies = 0;
                //пока добавление без боссов
                for (CompanyWithDownDependencies companyWithDownDependencies : companyWithDownDependenciesList) {
                    Company company = new Company(companyWithDownDependencies.getId(), companyWithDownDependencies.getName(),
                            companyWithDownDependencies.getDescription());
                    if (companyController.findOne(company.getId()) == null) {
                        companyController.insert(company);
                        numberOfInsertedCompanies++;
                    } else {
                        numberOfMissedCompanies++;
                    }
                    List<DepartmentWithDownDependencies> departmentWithDownDependenciesList = companyWithDownDependencies.getDepartmentWithDownDependencies();
                    numberOfDepartments += departmentWithDownDependenciesList.size();
                    for (DepartmentWithDownDependencies departmentWithDownDependencies : departmentWithDownDependenciesList) {
                        Department department = new Department(departmentWithDownDependencies.getId(),
                                departmentWithDownDependencies.getName(), departmentWithDownDependencies.getDescription(),
                                company, null);
                        if (departmentController.findOne(department.getId()) == null) {
                            departmentController.insert(department);
                            numberOfInsertedDepartments++;
                        } else {
                            numberOfMissedDepartments++;
                        }
                        List<Employee> employees = toDbListEmployee(departmentWithDownDependencies.getEmployees());
                        numberOfEmployees += employees.size();
                        for (Employee employee : employees) {
                            Employee employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(),
                                    department, null);
                            if (employeeController.findOne(employee.getId()) == null) {
                                employeeController.insert(employee1);
                                numberOfInsertedEmployees++;
                            } else {
                                numberOfMissedEmployees++;
                            }
                        }
                    }
                }
                statistics.setTotalCompanies(companyWithDownDependenciesList.size());
                statistics.setTotalDepartments(numberOfDepartments);
                statistics.setTotalEmployees(numberOfEmployees);
                statistics.setNumberOfCompanyInserted(numberOfInsertedCompanies);
                statistics.setNumberOfDepartmentInserted(numberOfInsertedDepartments);
                statistics.setNumberOfEmployeeInserted(numberOfInsertedEmployees);
                statistics.setNumberOfCompanyMissed(numberOfMissedCompanies);
                statistics.setNumberOfDepartmentMissed(numberOfMissedDepartments);
                statistics.setNumberOfEmployeeMissed(numberOfMissedEmployees);
            }
            //добавление боссов департаментам и сотрудникам
            for (CompanyWithDownDependencies companyWithDownDependencies : companyWithDownDependenciesList) {
                List<DepartmentWithDownDependencies> departmentWithDownDependenciesList = companyWithDownDependencies.getDepartmentWithDownDependencies();
                Company company = new Company(companyWithDownDependencies.getId(), companyWithDownDependencies.getName(),
                        companyWithDownDependencies.getDescription());
                for (DepartmentWithDownDependencies departmentWithDownDependencies : departmentWithDownDependenciesList) {
                    Department department = null;
                    if (departmentWithDownDependencies.getEmployees_boss() != 0) {
                        department = new Department(departmentWithDownDependencies.getId(),
                                departmentWithDownDependencies.getName(), departmentWithDownDependencies.getDescription(),
                                company, employeeController.findOne(departmentWithDownDependencies.getEmployees_boss()));
                        departmentController.update(department);
                    } else {
                        department = new Department(departmentWithDownDependencies.getId(),
                                departmentWithDownDependencies.getName(), departmentWithDownDependencies.getDescription(),
                                company, null);
                    }
                    List<model.xml.Employee> employees = departmentWithDownDependencies.getEmployees();
                    for (model.xml.Employee employee : employees) {
                        Employee employee1 = null;
                        if (employee.getBoss() != 0) {
                            employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(),
                                    department, employeeController.findOne(employee.getBoss()));
                            employeeController.update(employee1);
                        }
                    }
                }
            }
        }
        if (object instanceof Departments) {
            List<Department> departments = toDbListDepartment(((Departments) object).getDepartments());
            if (duplicate.equals("rewrite")) {
                int numberOfUpdated = 0;
                int numberOfInserted = 0;
                for (Department department : departments) {
                    Department department1 = new Department(department.getId(), department.getName(),
                            department.getDescription(), companyController.findOne(department.getCompany().getId()), null);
                    if (departmentController.findOne(department.getId()) == null) {
                        departmentController.insert(department1);
                        numberOfInserted++;
                    } else {
                        departmentController.delete(department1.getId());
                        departmentController.insert(department1);
                        numberOfUpdated++;
                    }
                }
                statistics.setTotalDepartments(departments.size());
                statistics.setNumberOfDepartmentInserted(numberOfInserted);
                statistics.setNumberOfDepartmentUpdated(numberOfUpdated);
            }
            if (duplicate.equals("miss")) {
                int numberOfMissed = 0;
                int numberOfInserted = 0;
                for (Department department : departments) {
                    Department department1 = new Department(department.getId(), department.getName(),
                            department.getDescription(), companyController.findOne(department.getCompany().getId()), null);
                    if (departmentController.findOne(department.getId()) == null) {
                        departmentController.insert(department1);
                        numberOfInserted++;
                    } else {
                        numberOfMissed++;
                    }
                }
                statistics.setTotalDepartments(departments.size());
                statistics.setNumberOfDepartmentInserted(numberOfInserted);
                statistics.setNumberOfDepartmentMissed(numberOfMissed);
            }
            for (Department department : departments) {
                Department department1 = null;
                if (department.getEmployee() != null) {
                    department1 = new Department(department.getId(), department.getName(), department.getDescription(),
                            companyController.findOne(department.getCompany().getId()),
                            employeeController.findOne(department.getEmployee().getId()));
                    departmentController.update(department1);
                }
            }
        }
        if (object instanceof DownDependencies) {
            List<DepartmentWithDownDependencies> departmentWithDownDependenciesList = ((DownDependencies) object).getDepartmentWithDownDependencies();
            if (duplicate.equals("rewrite")) {
                List<Employee> listFormerEmployees = new ArrayList<>();
                int numberOfUpdatedDepartments = 0;
                int numberOfInsertedDepartments = 0;
                int numberOfEmployees = 0;
                int numberOfUpdatedEmployees = 0;
                int numberOfInsertedEmployees = 0;
                for (DepartmentWithDownDependencies departmentWithDownDependencies : departmentWithDownDependenciesList) {
                    Department department = new Department(departmentWithDownDependencies.getId(), departmentWithDownDependencies.getName(),
                            departmentWithDownDependencies.getDescription(), companyController.findOne(departmentWithDownDependencies.getCompanies_id()),
                            null);
                    if (departmentController.findOne(department.getId()) == null) {
                        departmentController.insert(department);
                        numberOfInsertedDepartments++;
                    } else {
                        listFormerEmployees = employeeController.findByDepartmentId(department.getId());
                        departmentController.delete(department.getId());
                        departmentController.insert(department);
                        numberOfUpdatedDepartments++;
                    }
                    List<Employee> employees = toDbListEmployee(departmentWithDownDependencies.getEmployees());
                    numberOfEmployees += employees.size();
                    for (Employee employee : employees) {
                        Employee employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(),
                                department, null);
                        if (employeeController.findOne(employee.getId()) == null) {
                            boolean flag = false;
                            employeeController.insert(employee1);
                            for (Employee employee2 : listFormerEmployees) {
                                if (employee2.getId().equals(employee1.getId())) {
                                    numberOfUpdatedEmployees++;
                                    flag = true;
                                    break;
                                }
                            }
                            if (!flag) {
                                numberOfInsertedEmployees++;
                            }
                        } else {
                            employeeController.delete(employee1.getId());
                            employeeController.insert(employee1);
                            numberOfUpdatedEmployees++;
                        }
                    }
                }
                statistics.setTotalDepartments(departmentWithDownDependenciesList.size());
                statistics.setTotalEmployees(numberOfEmployees);
                statistics.setNumberOfDepartmentInserted(numberOfInsertedDepartments);
                statistics.setNumberOfDepartmentUpdated(numberOfUpdatedDepartments);
                statistics.setNumberOfEmployeeInserted(numberOfInsertedEmployees);
                statistics.setNumberOfEmployeeUpdated(numberOfUpdatedEmployees);
            }
            if (duplicate.equals("miss")) {
                int numberOfMissedDepartments = 0;
                int numberOfInsertedDepartments = 0;
                int numberOfEmployees = 0;
                int numberOfMissedEmployees = 0;
                int numberOfInsertedEmployees = 0;
                for (DepartmentWithDownDependencies departmentWithDownDependencies : departmentWithDownDependenciesList) {
                    Department department = new Department(departmentWithDownDependencies.getId(), departmentWithDownDependencies.getName(),
                            departmentWithDownDependencies.getDescription(), companyController.findOne(departmentWithDownDependencies.getCompanies_id()),
                            null);
                    if (departmentController.findOne(department.getId()) == null) {
                        departmentController.insert(department);
                        numberOfInsertedDepartments++;
                    } else {
                        numberOfMissedDepartments++;
                    }
                    List<Employee> employees = toDbListEmployee(departmentWithDownDependencies.getEmployees());
                    numberOfEmployees += employees.size();
                    for (Employee employee : employees) {
                        Employee employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(),
                                department, null);
                        if (employeeController.findOne(employee.getId()) == null) {
                            employeeController.insert(employee1);
                            numberOfInsertedEmployees++;
                        } else {
                            numberOfMissedEmployees++;
                        }
                    }
                }
                statistics.setTotalDepartments(departmentWithDownDependenciesList.size());
                statistics.setTotalEmployees(numberOfEmployees);
                statistics.setNumberOfDepartmentInserted(numberOfInsertedDepartments);
                statistics.setNumberOfDepartmentMissed(numberOfMissedDepartments);
                statistics.setNumberOfEmployeeInserted(numberOfInsertedEmployees);
                statistics.setNumberOfEmployeeMissed(numberOfMissedEmployees);
            }
            for (DepartmentWithDownDependencies departmentWithDownDependencies : departmentWithDownDependenciesList) {
                Department department = null;
                if (departmentWithDownDependencies.getEmployees_boss() != 0) {
                    department = new Department(departmentWithDownDependencies.getId(), departmentWithDownDependencies.getName(),
                            departmentWithDownDependencies.getDescription(), companyController.findOne(departmentWithDownDependencies.getCompanies_id()),
                            employeeController.findOne(departmentWithDownDependencies.getEmployees_boss()));
                    departmentController.update(department);
                } else {
                    department = new Department(departmentWithDownDependencies.getId(), departmentWithDownDependencies.getName(),
                            departmentWithDownDependencies.getDescription(), companyController.findOne(departmentWithDownDependencies.getCompanies_id()),
                            null);
                }
                List<Employee> employees = toDbListEmployee(departmentWithDownDependencies.getEmployees());
                for (Employee employee : employees) {
                    if (employee.getEmployee() != null) {
                        Employee employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(),
                                department, employeeController.findOne(employee.getEmployee().getId()));
                        employeeController.update(employee1);
                    }
                }
            }
        }
        if (object instanceof UpDependencies) {
            List<DepartmentWithUpDependencies> departmentWithUpDependenciesList = ((UpDependencies) object).getDepartmentWithUpDependencies();
            if (duplicate.equals("rewrite")) {
                List<Department> listFormerDepartments = new ArrayList<>();
                int numberOfUpdatedDepartments = 0;
                int numberOfInsertedDepartments = 0;
                int numberOfDepartments = 0;
                int numberOfUpdatedCompanies = 0;
                int numberOfInsertedCompanies = 0;
                for (DepartmentWithUpDependencies departmentWithUpDependencies : departmentWithUpDependenciesList) {
                    Company company = new Company(departmentWithUpDependencies.getId(), departmentWithUpDependencies.getName(),
                            departmentWithUpDependencies.getDescription());
                    if (companyController.findOne(company.getId()) == null) {
                        companyController.insert(company);
                        numberOfInsertedCompanies++;
                    } else {
                        listFormerDepartments = departmentController.findByCompanyId(company.getId());
                        companyController.delete(company.getId());
                        companyController.insert(company);
                        numberOfUpdatedCompanies++;
                    }
                    Department department = toDbDepartment(departmentWithUpDependencies.getDepartment());
                    Department department1 = new Department(department.getId(), department.getName(),
                            department.getDescription(), company, null);
                    numberOfDepartments++;
                    if (departmentController.findOne(department.getId()) == null) {
                        boolean flag = false;
                        departmentController.insert(department1);
                        for (Department department2 : listFormerDepartments) {
                            if (department2.getId().equals(department1.getId())) {
                                numberOfUpdatedDepartments++;
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) {
                            numberOfInsertedDepartments++;
                        }
                    } else {
                        departmentController.delete(department1.getId());
                        departmentController.insert(department1);
                        numberOfUpdatedDepartments++;
                    }
                }
                statistics.setTotalDepartments(numberOfDepartments);
                statistics.setTotalCompanies(departmentWithUpDependenciesList.size());
                statistics.setNumberOfDepartmentInserted(numberOfInsertedDepartments);
                statistics.setNumberOfDepartmentUpdated(numberOfUpdatedDepartments);
                statistics.setNumberOfCompanyInserted(numberOfInsertedCompanies);
                statistics.setNumberOfCompanyUpdated(numberOfUpdatedCompanies);
            }
            if (duplicate.equals("miss")) {
                int numberOfMissedDepartments = 0;
                int numberOfInsertedDepartments = 0;
                int numberOfDepartments = 0;
                int numberOfMissedCompanies = 0;
                int numberOfInsertedCompanies = 0;
                for (DepartmentWithUpDependencies departmentWithUpDependencies : departmentWithUpDependenciesList) {
                    Company company = new Company(departmentWithUpDependencies.getId(), departmentWithUpDependencies.getName(),
                            departmentWithUpDependencies.getDescription());
                    if (companyController.findOne(company.getId()) == null) {
                        companyController.insert(company);
                        numberOfInsertedCompanies++;
                    } else {
                        numberOfMissedCompanies++;
                    }
                    Department department = toDbDepartment(departmentWithUpDependencies.getDepartment());
                    Department department1 = new Department(department.getId(), department.getName(),
                            department.getDescription(), company, null);
                    numberOfDepartments++;
                    if (departmentController.findOne(department.getId()) == null) {
                        departmentController.insert(department1);
                        numberOfInsertedDepartments++;
                    } else {
                        numberOfMissedDepartments++;
                    }
                }
                statistics.setTotalDepartments(numberOfDepartments);
                statistics.setTotalCompanies(departmentWithUpDependenciesList.size());
                statistics.setNumberOfDepartmentInserted(numberOfInsertedDepartments);
                statistics.setNumberOfDepartmentMissed(numberOfMissedDepartments);
                statistics.setNumberOfCompanyInserted(numberOfInsertedCompanies);
                statistics.setNumberOfCompanyMissed(numberOfMissedCompanies);
            }
            for (DepartmentWithUpDependencies departmentWithUpDependencies : departmentWithUpDependenciesList) {
                Company company = new Company(departmentWithUpDependencies.getId(), departmentWithUpDependencies.getName(),
                        departmentWithUpDependencies.getDescription());
                Department department = toDbDepartment(departmentWithUpDependencies.getDepartment());
                if (department.getEmployee() != null) {
                    Department department1 = new Department(department.getId(), department.getName(), department.getDescription(),
                            company, employeeController.findOne(department.getEmployee().getId()));
                    departmentController.update(department1);
                }
            }
        }
        if (object instanceof Employees) {
            List<Employee> employees = toDbListEmployee(((Employees) object).getEmployees());
            if (duplicate.equals("rewrite")) {
                int numberOfUpdated = 0;
                int numberOfInserted = 0;
                for (Employee employee : employees) {
                    Employee employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(),
                            departmentController.findOne(employee.getDepartment().getId()), null);
                    if (employeeController.findOne(employee.getId()) == null) {
                        employeeController.insert(employee1);
                        numberOfInserted++;
                    } else {
                        employeeController.delete(employee1.getId());
                        employeeController.insert(employee1);
                        numberOfUpdated++;
                    }
                }
                statistics.setTotalEmployees(employees.size());
                statistics.setNumberOfEmployeeInserted(numberOfInserted);
                statistics.setNumberOfEmployeeUpdated(numberOfUpdated);
            }
            if (duplicate.equals("miss")) {
                int numberOfMissed = 0;
                int numberOfInserted = 0;
                for (Employee employee : employees) {
                    Employee employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(),
                            departmentController.findOne(employee.getDepartment().getId()), null);
                    if (employeeController.findOne(employee.getId()) == null) {
                        employeeController.insert(employee1);
                        numberOfInserted++;
                    } else {
                        numberOfMissed++;
                    }
                }
                statistics.setTotalEmployees(employees.size());
                statistics.setNumberOfEmployeeInserted(numberOfInserted);
                statistics.setNumberOfEmployeeMissed(numberOfMissed);
            }
            for (Employee employee : employees) {
                if (employee.getEmployee() != null) {
                    Employee employee1 = new Employee(employee.getId(), employee.getName(), employee.getSurname(),
                            departmentController.findOne(employee.getDepartment().getId()),
                            employeeController.findOne(employee.getEmployee().getId()));
                    employeeController.update(employee1);
                }
            }
        }
        return statistics;
    }

    @Override
    public void setSessionContext(SessionContext sessionContext) {

    }

    @Override
    public void ejbRemove() {

    }

    @Override
    public void ejbActivate() {

    }

    @Override
    public void ejbPassivate() {

    }

    public void ejbCreate() {

    }
}
