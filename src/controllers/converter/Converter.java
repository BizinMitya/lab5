package controllers.converter;

import model.Company;
import model.Department;
import model.Employee;
import model.xml.Statistics;

import javax.ejb.EJBObject;
import javax.naming.NamingException;
import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.Serializable;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Dmitriy on 16.04.2016.
 */
public interface Converter extends EJBObject, Serializable {
    File exportDb(String type, List<Integer> ids, String dependence, String view) throws RemoteException;

    Statistics importDb(File file, String duplicate) throws RemoteException, JAXBException, SQLException, NamingException;

    model.xml.Company toXmlCompany(Company company) throws RemoteException;

    Company toDbCompany(model.xml.Company company) throws RemoteException;

    model.xml.Department toXmlDepartment(Department department) throws RemoteException;

    Department toDbDepartment(model.xml.Department department) throws RemoteException;

    model.xml.Employee toXmlEmployee(Employee employee) throws RemoteException;

    Employee toDbEmployee(model.xml.Employee employee) throws RemoteException;

    List<model.xml.Company> toXmlListCompany(List<Company> companies) throws RemoteException;

    List<Company> toDbListCompany(List<model.xml.Company> companies) throws RemoteException;

    List<model.xml.Department> toXmlListDepartment(List<Department> departments) throws RemoteException;

    List<Department> toDbListDepartment(List<model.xml.Department> departments) throws RemoteException;

    List<model.xml.Employee> toXmlListEmployee(List<Employee> employees) throws RemoteException;

    List<Employee> toDbListEmployee(List<model.xml.Employee> employees) throws RemoteException;
}
