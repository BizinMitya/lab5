package controllers.converter;

import javax.ejb.CreateException;
import javax.ejb.EJBHome;
import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * Created by Dmitriy on 16.04.2016.
 */
public interface ConverterHome extends EJBHome, Serializable {
    Converter create() throws CreateException, RemoteException;
}
