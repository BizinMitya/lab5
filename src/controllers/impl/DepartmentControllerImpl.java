package controllers.impl;

import controllers.DepartmentController;
import controllers.HibernateUtil;
import model.Department;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 24.02.2016.
 */
public class DepartmentControllerImpl implements DepartmentController {
    public static final String FIND_ALL_QUERY = "FROM Department";
    public static final String DELETE = "DELETE FROM Department WHERE id = :id";
    public static final String UPDATE = "UPDATE Department SET name = :name, description = :description, company = :company, employee = :employee WHERE id = :id";
    public static final String FIND_BY_PARAMETERS = "FROM Department WHERE id = :id OR name = :name OR description = :description";
    public static final String FIND_BY_LIKE_ID = "SELECT id FROM Department WHERE id LIKE ";
    public static final String FIND_BY_LIKE_NAME = "SELECT name FROM Department WHERE name LIKE ";
    public static final String FIND_BY_LIKE_DESCRIPTION = "SELECT description FROM Department WHERE description LIKE ";

    @Override
    public List<Department> findByParameters(String id, String name, String description) {
        Session session = getSession();
        Query query = session.createQuery(FIND_BY_PARAMETERS);
        if (!id.equals("")) {
            query.setInteger("id", Integer.parseInt(id));
        } else {
            query.setString("id", "");
        }
        query.setString("name", name);
        query.setString("description", description);
        List<Department> arrayList = (List<Department>) query.list();
        session.close();
        return arrayList;
    }


    private Session getSession() {
        return HibernateUtil.createSessionFactory().openSession();
    }

    @Override
    public List<Department> findAll() {
        Session session = getSession();
        List<Department> arrayList = (List<Department>) session.createQuery(FIND_ALL_QUERY).list();
        session.close();
        return arrayList;
    }

    @Override
    public Department findOne(Integer id) {
        Session session = getSession();
        Department department = (Department) session.get(Department.class, id);
        session.close();
        return department;
    }

    @Override
    public void insert(Department department) throws NamingException, SQLException {
        Session session = getSession();
        session.save(department);
        session.flush();
        session.close();
    }

    @Override
    public void delete(int id) throws NamingException, SQLException {
        Session session = getSession();
        Query query = session.createQuery(DELETE);
        query.setParameter("id", id);
        query.executeUpdate();
        session.close();
    }

    @Override
    public void update(Department department) throws NamingException, SQLException {
        Session session = getSession();
        Query query = session.createQuery(UPDATE);
        query.setParameter("name", department.getName());
        query.setParameter("description", department.getDescription());
        query.setParameter("company", department.getCompany());
        query.setParameter("employee", department.getEmployee());
        query.setParameter("id", department.getId());
        query.executeUpdate();
        session.close();
    }

    @Override
    public List<Department> findByIds(List<Integer> ids) {
        List<Department> departments = new ArrayList<>();
        for (Integer i : ids) {
            departments.add(findOne(i));
        }
        return departments;
    }

    @Override
    public List<Department> findByCompanyId(Integer companyId) {
        List<Department> departments = findAll();
        List<Department> result = new ArrayList<>();
        for (int i = 0; i < departments.size(); i++) {
            if (departments.get(i).getCompany().getId().equals(companyId)) {
                result.add(departments.get(i));
            }
        }
        return result;
    }

    @Override
    public List<String> findByLikeParameter(String parameter, String like) {
        List<String> result = null;
        Session session = getSession();
        Query query = null;
        if (parameter.equals("id")) {
            query = session.createQuery(FIND_BY_LIKE_ID + "'" + like + "%'");
        }
        if (parameter.equals("name")) {
            query = session.createQuery(FIND_BY_LIKE_NAME + "'" + like + "%'");
        }
        if (parameter.equals("description")) {
            query = session.createQuery(FIND_BY_LIKE_DESCRIPTION + "'" + like + "%'");
        }
        if (parameter.equals("id")) {
            result = new ArrayList<>();
            List<Integer> ids = (List<Integer>) query.list();
            for (Integer i : ids) {
                result.add(String.valueOf(i));
            }
        } else {
            result = (List<String>) query.list();
        }
        session.close();
        return result;
    }
}
