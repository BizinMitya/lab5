package controllers.impl;

import controllers.CompanyController;
import controllers.HibernateUtil;
import model.Company;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 24.02.2016.
 */
public class CompanyControllerImpl implements CompanyController {
    public static final String FIND_ALL_QUERY = "FROM Company";
    public static final String DELETE = "DELETE FROM Company WHERE id = :id";
    public static final String UPDATE = "UPDATE Company SET name = :name, description = :description WHERE id = :id";
    public static final String FIND_BY_PARAMETERS = "FROM Company WHERE id = :id OR name = :name OR description = :description";
    public static final String FIND_BY_LIKE_ID = "SELECT id FROM Company WHERE id LIKE ";
    public static final String FIND_BY_LIKE_NAME = "SELECT name FROM Company WHERE name LIKE ";
    public static final String FIND_BY_LIKE_DESCRIPTION = "SELECT description FROM Company WHERE description LIKE ";

    @Override
    public List<Company> findByParameters(String id, String name, String description) {
        Session session = getSession();
        Query query = session.createQuery(FIND_BY_PARAMETERS);
        if (!id.equals("")) {
            query.setInteger("id", Integer.parseInt(id));
        } else {
            query.setString("id", "");
        }
        query.setString("name", name);
        query.setString("description", description);
        List<Company> arrayList = (List<Company>) query.list();
        session.close();
        return arrayList;
    }

    @Override
    public void insert(Company company) throws NamingException, SQLException {
        Session session = getSession();
        session.save(company);
        session.flush();
        session.close();
    }

    private Session getSession() {
        return HibernateUtil.createSessionFactory().openSession();
    }

    @Override
    public void delete(int id) throws NamingException, SQLException {
        Session session = getSession();
        Query query = session.createQuery(DELETE);
        query.setParameter("id", id);
        query.executeUpdate();
        session.close();
    }

    @Override
    public void update(Company company) throws NamingException, SQLException {
        Session session = getSession();
        Query query = session.createQuery(UPDATE);
        query.setParameter("name", company.getName());
        query.setParameter("description", company.getDescription());
        query.setParameter("id", company.getId());
        query.executeUpdate();
        session.close();
    }

    @Override
    public List<Company> findAll() {
        Session session = getSession();
        List<Company> arrayList = (List<Company>) session.createQuery(FIND_ALL_QUERY).list();
        session.close();
        return arrayList;
    }

    @Override
    public Company findOne(Integer id) {
        Session session = getSession();
        Company company = (Company) session.get(Company.class, id);
        session.close();
        return company;
    }

    @Override
    public List<Company> findByIds(List<Integer> ids) {
        List<Company> companies = new ArrayList<>();
        for (Integer i : ids) {
            companies.add(findOne(i));
        }
        return companies;
    }

    @Override
    public List<String> findByLikeParameter(String parameter, String like) {
        List<String> result = null;
        Session session = getSession();
        Query query = null;
        if (parameter.equals("id")) {
            query = session.createQuery(FIND_BY_LIKE_ID + "'" + like + "%'");
        }
        if (parameter.equals("name")) {
            query = session.createQuery(FIND_BY_LIKE_NAME + "'" + like + "%'");
        }
        if (parameter.equals("description")) {
            query = session.createQuery(FIND_BY_LIKE_DESCRIPTION + "'" + like + "%'");
        }
        if (parameter.equals("id")) {
            result = new ArrayList<>();
            List<Integer> ids = (List<Integer>) query.list();
            for (Integer i : ids) {
                result.add(String.valueOf(i));
            }
        } else {
            result = (List<String>) query.list();
        }
        session.close();
        return result;
    }
}
