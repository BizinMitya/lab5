package controllers.impl;

import controllers.EmployeeController;
import controllers.HibernateUtil;
import model.Employee;
import org.hibernate.Query;
import org.hibernate.Session;

import javax.naming.NamingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 27.02.2016.
 */
public class EmployeeControllerImpl implements EmployeeController {
    public static final String FIND_ALL_QUERY = "FROM Employee";
    public static final String DELETE = "DELETE FROM Employee WHERE id = :id";
    public static final String UPDATE = "UPDATE Employee SET name = :name, surname = :surname, department = :department, employee = :employee WHERE id = :id";
    public static final String FIND_BY_PARAMETERS = "FROM Employee WHERE id = :id OR name = :name OR surname = :surname";
    public static final String FIND_BY_LIKE_ID = "SELECT id FROM Employee WHERE id LIKE ";
    public static final String FIND_BY_LIKE_NAME = "SELECT name FROM Employee WHERE name LIKE ";
    public static final String FIND_BY_LIKE_SURNAME = "SELECT surname FROM Employee WHERE surname LIKE ";

    private List<Employee> result = new ArrayList<>();

    //Сам себе босс или без босса - основа,корень
    @Override
    public void helper(int parent, List<Employee> employees, int idDepartment) {
        for (Employee employee : employees) {
            if (employee.getEmployee() != null && employee.getEmployee().getId() == parent && employee.getId() != parent/* && employee.getDepartments_id() == idDepartment*/) {
                //idDepartment - чтобы дерево внутри департамента было(сделали вообще общее дерево)
                getResult().add(employee);
                helper(employee.getId(), employees, idDepartment);
            }
        }
    }

    @Override
    public StringBuffer buildTree(List<Employee> employees, int parent, StringBuffer stringBuffer) {
        stringBuffer.append("<ul>");
        for (Employee employee : employees) {
            if (employee.getEmployee() != null && employee.getEmployee().getId() == parent) {
                stringBuffer.append("<li><a href=employees.jsp?id=").append(employee.getId()).append(">").append(employee.getName()).append(" ").append(employee.getSurname() + "</a>");
                buildTree(employees, employee.getId(), stringBuffer);
                stringBuffer.append("</li>");
            }
        }
        stringBuffer.append("</ul>");
        return stringBuffer;
    }


    private Session getSession() {
        return HibernateUtil.createSessionFactory().openSession();
    }

    @Override
    public List<Employee> findByParameters(String id, String name, String surname) {
        Session session = getSession();
        Query query = session.createQuery(FIND_BY_PARAMETERS);
        if (!id.equals("")) {
            query.setInteger("id", Integer.parseInt(id));
        } else {
            query.setString("id", "");
        }
        query.setString("name", name);
        query.setString("surname", surname);
        List<Employee> arrayList = (List<Employee>) query.list();
        session.close();
        return arrayList;
    }

    @Override
    public List<Employee> findAll() {
        Session session = getSession();
        List<Employee> arrayList = (List<Employee>) session.createQuery(FIND_ALL_QUERY).list();
        session.close();
        return arrayList;
    }

    @Override
    public Employee findOne(Integer id) {
        Session session = getSession();
        Employee employee = (Employee) session.get(Employee.class, id);
        session.close();
        return employee;
    }

    @Override
    public void insert(Employee employee) throws NamingException, SQLException {
        Session session = getSession();
        session.save(employee);
        session.flush();
        session.close();
    }


    @Override
    public void delete(int id) throws NamingException, SQLException {
        Session session = getSession();
        Query query = session.createQuery(DELETE);
        query.setParameter("id", id);
        query.executeUpdate();
        session.close();
    }

    @Override
    public void update(Employee employee) throws NamingException, SQLException {
        Session session = getSession();
        Query query = session.createQuery(UPDATE);
        query.setParameter("name", employee.getName());
        query.setParameter("surname", employee.getSurname());
        query.setParameter("department", employee.getDepartment());
        query.setParameter("employee", employee.getEmployee());
        query.setParameter("id", employee.getId());
        query.executeUpdate();
        session.close();
    }

    @Override
    public List<Employee> findByIds(List<Integer> ids) {
        List<Employee> employees = new ArrayList<>();
        for (Integer i : ids) {
            employees.add(findOne(i));
        }
        return employees;
    }

    @Override
    public List<Employee> findByDepartmentId(Integer departmentId) {
        List<Employee> employees = findAll();
        List<Employee> result = new ArrayList<>();
        for (int i = 0; i < employees.size(); i++) {
            if (employees.get(i).getDepartment().getId().equals(departmentId)) {
                result.add(employees.get(i));
            }
        }
        return result;
    }

    @Override
    public List<String> findByLikeParameter(String parameter, String like) {
        List<String> result = null;
        Session session = getSession();
        Query query = null;
        if (parameter.equals("id")) {
            query = session.createQuery(FIND_BY_LIKE_ID + "'" + like + "%'");
        }
        if (parameter.equals("name")) {
            query = session.createQuery(FIND_BY_LIKE_NAME + "'" + like + "%'");
        }
        if (parameter.equals("surname")) {
            query = session.createQuery(FIND_BY_LIKE_SURNAME + "'" + like + "%'");
        }
        if (parameter.equals("id")) {
            result = new ArrayList<>();
            List<Integer> ids = (List<Integer>) query.list();
            for (Integer i : ids) {
                result.add(String.valueOf(i));
            }
        } else {
            result = (List<String>) query.list();
        }
        session.close();
        return result;
    }

    @Override
    public List<Employee> getResult() {
        return result;
    }
}

